# Importação de arquivos

De forma a compor a cena é possível importar diferentes tipos de arquivos por meio do menu Arquivo>Importar. É importante ressaltar que, apesar de muitos tipos de arquivo serem importados e visualizados corretamente no editor, nem todos eles poderão ser utilizados no tour virtual final. Em resumo, o deitor aceita mais formatos de arquivo do que o visualizador de tour virtual.

## Arquivos aceitos no tour virtual

1. GLB: Arquivos glb incluindo arquivos com compressão draco são os mais indicados para serem utilizados na composição das cenas. Sua principal vantagem é o seu tamanho reduzido sem perda de qualidade sendo muito adequado para uso na internet. A princípio boa parte dos formatos podem ser convertidos para esse formato utilizando softwares intermediários como o Blender e o gltf-pipeline (esse último permite uma grande compressão do arquivo).

2. FBX: Formato comum de exportação e compatível com muitas plataformas. Não é tão otimizado em uso de espaço e, no editor, não importa textura automaticamente. Além da menor compressão esse arquivo exige que as texturas sejam copiadas para a pasta de modelos após a exportação.

3. Imagens: Arquivos de imagem comum (JPG, PNG) podem ser importados diretamente no menu importar. Por padrão eles são importados preservando as proporções originais e são adicionados automaticamente no zip exportado sem necessidade de inserção manual.

## Abrindo arquivos de projeto

O menu de importação também é utilizado para importar arquivos de projeto salvos pelo editor no formato JSON. Ao importar arquivos desse tipo todos os dados de script, chaves de animação e slides são restaurados.
