# Menu de edição - cena

![cena](./img/Scene.PNG)

O menu de cena possui duas partes principais: Árvore de objetos e Controle de Ambiente.

## Árvore de objetos

Apresenta todos os objetos disponíveis na cena seguindo a seguinte convenção:

- Objetos filho são destacados por meio de tabulação. Apenas a cena, grupos adicionados pelo menu adicionar e objetos importados expandidos possuem filhos.

- A câmera é o único objeto independente da cena

A árvore de objetos pode ser utilizada para selecionar objetos, ela é, inclusive, a única forma de selecionar objetos filho da cena. Além da função de seleção é possível mover objetos dentro da árvore através da operação de clicar e arrastar. Esse método é utilizado para inserir objetos filho nos grupos adicionados.

Outra forma de mudar o foco da seleção é utilizando as setas do teclado para cima/baixo.

## Controle de Ambiente

O controle de ambiente permite que uma textura de ambiente seja escolhida para compor a cena e facilitar a visualização de objetos 3D na cena. Esse tipo de configuração influencia apenas na aparência do editor, o resultado final terá por padrão uma composição neutra de ambiente.

O menu de controle de ambiente oferece 3 opções de escolha detalhadas a seguir.

---

### Sem textura de fundo

A primeira opção é deixar o plano de fundo sem dados de textura. Nesse caso formas tridimensionais irão aparecer sem sombras, parecendo recortes bidimensionais. Uma forma de visualizar bem o efeito do plano de fundo é utilizar um material reflexivo metálico de exemplo, nesse caso a imagem do objeto 3d será vista, nessa primeira opção, da seguinte maneira:

![viewport - no texture](./img/viewport.PNG)

---

### Com textura customizada de fundo

A segunda opção é deixar o plano de fundo com uma textura customizada. Quando essa opção é selecionada aparece um botão retangular ao lado da caixa de seleção que permite ao usuário selecionar uma imagem na memória que possa ser utilizada como textura de fundo. No caso do coelho metalizado haverá uma reflexão que corresponderá à textura carregada:

![viewport - no texture](img/viewport_texture.PNG)

---

### Vis. Modelo

A terceira e última opção é utilizar um plano de fundo neutro que permita uma boa visualização da forma tridimesional dos objetos. Quando essa opção é selecionada o sombreamento e as cores passam a ser calculadas a partir desse padrão neutro pré-estabelecido. No caso do coelho metalizado haverá uma reflexão metálica neutra:

![viewport - no texture](img/viewport_vismodel.PNG)
