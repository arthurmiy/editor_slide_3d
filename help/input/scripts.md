# Scripts

De forma a expandir ainda mais as possibilidade do tour 3d é possível adicionar Scripts e fazer uso de alguns callbacks de eventos importantes durante a execução da página final.

## Operações disponíveis

O menu de script permite apenas duas operações: edição e reset.

Vale ressaltar que o código do script **_deve_** conter os callbacks padrão para que o tour virtual exportado funcione corretamente. O botão reset serve exatamente para recuperar o código básico que permite o funcionamento do tour virtual.

O botão editar faz com que a janela de edição seja aberta. Nessa janela há uma área que permite a inserção de código javascript que irá compor o resultado final. O editor apresentado apresenta uma avaliação preliminar de erros básicos de código em tempo de edição, sendo assim, alguns erros poderão ser corrigidos sem necessidade de teste direto no produto final.

![script menu](img/script.PNG)

## Código

![script menu](img/scriptEdit.PNG)

O código básico apresentado na figura acima possui alguns comentários que permitem um entendimento básico de cada função. De qualquer maneira as funções serão explicadas a seguir

### OnSlideChange

Essa função é chamada toda vez em que há troca de slide. Ela oferece como dado os parâmetros oldSlide e newSlide que são os indices dos slides (iniciando em zero) anterior e atual respectivamente.

### OnSlidePositionUpdate

Essa função é chamada a cada alteração na posição da página. As informações são passadas a partir do único objeto disponibilizado por ela: scrollInfos. As informações podem ser extraídas do objeto da mesma forma que se faz em qualquer objeto em javascript, digitando nome.propriedade.

Propriedades disponíveis:

- scrollInfos.index: Retorna o valor do índice do slide atual
- scrollInfos.percent: Retorna o posicionamento dentro do slide atual. O valor é dado em relação à porcentagem de saída, se o slide estiver totalmente visível a porcentagem é zero, caso tenha saído totalmente da tela a porcentagem é 100.
