# Área de visualização (Viewport)

A área de visualização, também chamada de viewport, é responsável pela exibição e captura de poses no editor. De maneira geral todas as poses são salvas a partir do ponto de vista da viewport, sendo assim, ao criar novos slides, a posição corrente da câmera da viewport é salva e atrelada ao slide que está sendo criado.

A viewport apresenta ainda estatisticas de uso da cena como, por exemplo a quantidade de objetos, vertices e triângulos presentes na cena.

## Navegação na viewport

As operações de navegação na viewport são as seguintes:

- Botão esquerdo do mouse : Rotação de câmera, muda o sentido da câmera na viewport permitindo ajudtes no ponto de vista de interesse.
- Botão direito do mouse : Operação de pan. Permite arrastar o modelo no sentido de movimentação do mouse.
- Scroll : O scroll permite que seja dado zoom in e zoom out no modelo.
- Clicar e arrastar scroll : outra forma de dar zoom in e out.

Além dessas formas básicas de navegação é possível alterar os parâmetros da câmera selecionando-a na árvore de objetos da cena e alterar posicionamento, rotação, etc. por lá.

## Operações na Viewport

Na viewport é possível selecionar objetos e, a partir do menu inferior, editar o posicionamento, escala e rotação do objeto selecionado. A seleção funciona, por padrão apenas para objetos pais.
