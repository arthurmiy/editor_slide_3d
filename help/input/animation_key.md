# Chaves de animação (Animation key)

As chaves de animação permitem salvar o estado de algum objeto de forma que, quando for pertinente, ele possa ser animado até assumir o estados desejado.

As chaves de animação são salvas por objeto e ficam visíveis todas as vezes que um objeto é selecionado.

O menu da chave de animação pode ser observado na seguinte imagem:

![menu animation key](./img/animationKey.PNG)

## Opções

No menu principal estão disponíveis as seguintes opções:

- **Novo:** cria uma nova chave de animação no objeto selecionado
- **Editar:** permite a edição da chave de animação. Ao ser clicado, abre a janela de Edição e altera a forma do objeto selecionado para refletir a pose salva.
- **Remover:** exclui a respsctiva chave de animação
- **Nome:** É possível alterar o nome da chave alterando o conteúdo da caixa de texto. O nome pode ser utilizado para facilitar a identificação de uma chave e, no caso da chave default, identifica a posição padrão do objeto.

## Dinâmica de animação - usando as chaves

As chaves de animação podem ser utilizadas em diferentes contextos de composição de cena.

A maneira mais simples de utilizar uma chave de animação é atribuindo ela como ação na entrada de um slide (mais informações [aqui](slides.md)). Por padrão a chave de animação utilizada é a que possui o nome "default", nos casos em que não há chaves com esse nome a primeira chave inserida é usada como chave padrão.

As chaves de animação também podem ser utilizadas nos scripts. O uso detalhado das chaves para essa aplicação pode ser encontrado [aqui](scripts.md)

## Menu de Edição

![menu de edição](./img/animationKeyEdit.PNG)

O menu de edição mostrado na figura acima se propões a configurar uma pose para o modelo selecionado.

Na parte superior fica indicado o nome da chave de animação que está sendo configurada

Na área de parâmetros da chave são configurados os itens que irão compor a pose. Cada um deles possui uma caixa de seleção que, quando selecionada, determina que o respectivo componente irá compor a pose. Podemos encontrar os seguintes parâmetros

- **Posição:** define a posição do objeto na chave de animação
- **Rotação:** define a rotação do objeto na chave de animação
- **Escala:** define a escala do objeto na chave de animação. Possui uma caixa de seleção opcional que permite que a mudança de escala se reflita proporcionalmente em todos os eixos.
- **Opacidade:** define a opacidade do objeto variando entre 0 e 1, sendo zero totalmente transparente e 1 totalmente opaco.
- **Destaque:** define a intensidade do destaque de cor atribuido ao objeto. O destaque varia de 0 a 1, sendo zero o objeto sem destaque de cor (cor padrão) e 1 o destaque máximo com o objeto irradiando uma cor vermelha viva.
