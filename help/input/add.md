# Menu Adicionar

O menu adicionar permite a inclusão de objetos pré-definidos que podem ser utilizados como suporte no momento de criação de cenários.

Existem três tipos de objetos disponibilizados por padrão: formas fixas, formas editáveis e grupo.

## Grupo

O grupo é um elemento diferente dos demais pois seu uso é voltado para a organização dos objetos. Como seu nome sugere ele permite o agrupamento de elementos e, dessa forma, organizar melhor a árvore de objetos.

Outro uso possível do grupo é a possíbilidade de mudar a escala e o posicionamento dos seus elementos de maneira conjunta, assim como a sua duplicação para uso em outro local. Da mesma maneira é possível organizar elementos em grupos para criar uma animação em conjunto.

---

### Visualização na Viewport

O grupo é o único elemento cujos filhos são exibidos na viewport. Quando um dos elementos filhos é selecionado na viewport, por padrão, é o grupo que vai receber o foco.

Caso haja necessidade de selecionar elementos filhos de um grupo, é preciso clicar na árvore de elementos da cena.

---

### Adicionando Elementos no grupo

Para adicionar elementos em um grupo basta arrastar o respectivo elemento para dentro do grupo na árvore de elementos.

---

## Formas Fixas

As formas fixas, apesar de sua denominação, permitem algumas alterações básicas de forma, entretanto, nada que altere consideravelmente sua base.

- **Caixa**: insere uma caixa cujas dimensões e subdivisões são editáveis na aba de geometria
- **Circulo**: Insere um círculo. A quantidade de subdivisões, o raio e os angulos de início/fim são editáveis.
- **Cilindro**: Permite a edição das proporções e da quantidade de segmentos
- **Dodecaedro**: Permite mudar o raio e o nível de detalhe
- **Icosaedro**: Permite mudar o raio e o nível de detalhe
- **Octaedro**: Permite mudar o raio e o nível de detalhe
- **Plano**: Permite a edição das proporções e da quantidade de segmentos
- **Anel**: Permite a edição das proporções e da quantidade de segmentos
- **Esfera**: Insere uma esfera. A quantidade de subdivisões, o raio e os angulos de início/fim são editáveis.
- **Sprite**: Insere uma imagem que se sobrepõe aos outros elementos e que sempre aparece alinhada com a câmera.
- **Tetraedro**: Permite a edição das proporções e da quantidade de segmentos
- **Toroide**: Permite a edição das proporções e da quantidade de segmentos

## Formas editáveis

- **Torno**: Permite a inserção de formas personalizadas. Basicamente a forma é determinada por uma lista de pares raio/altura. Entre dois elementos da lista é inserida uma superfície.
- **Tubo**: Permite a inserção de tubos ou cabos a partir de uma lista de posições. As posições dessa lista basicamente indicam os pontos pelos quais o tubo irá passar de maneira sequencial.
- **Texto**: Permite a inserção de textos tridimensionais.
- **Nó Toral**: Permite a criação de nós. Possui o parâmetro P que indica a quantidade de voltas e o parêmtro Q que indica a quantidade de nós dentro da volta
