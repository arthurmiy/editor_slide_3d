# Salvar Projeto

O projeto pode ser salvo para edição em outros computadores ou mesmo para edição posterior sem riscos de perda de dados.

O formato padrão de arquivo é o json, e contempla todos os dados necessários para recriar a cena sem a necessidade de importar novamente os modelos adicionados externamente.

O nome do arquivo json criado depende diretamente do nome atribuido ao projeto no respectivo menu. Para abrir novamente o arquivo json salvo, bastar utilizar o menu importar e selecionar o arquivo desejado no disco.
