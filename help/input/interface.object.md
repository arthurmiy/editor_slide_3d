# Menu de edição - Objeto

![edição de objeto](./img/objectProperties.PNG)

O menu de objetos permite a visualização e configuração de objetos presentes na cena.

A seguir estão detalhados os parâmetros apresentados nesse menu:

- **Tipo:** Mostra o tipo de objeto selecionado.
- **Botão expand:** O botão expand pode ser utilizado para modelos importados. Muitas vezes esses modelos são compostos por formas tridimensionais agrupadas dentro de um objeto do tipo grupo. O editor não permite que esses modelos internos sejam editados, entretanto, pode ser necessário ter acesso a eles de forma a configurar as texturas. Para que isso possa ser feito foi criado o botão expand que expande a árvore de objetos tornando os filhos do respectivo grupo acessíveis.
- **UUID:** pode ser alterado para diferentes objetos e, de maneira geral, serve para identificar cada objeto de maneira exclusiva. Esse tipo de identificação é importante principalmente para programar animações via script
- **Name:** Identifica um objeto. No caso de um objeto importado o nome deve corresponder ao nome do arquivo original (incluindo extensão).
- **Posição/Rotação/Escala:** São configurações básicas do objeto que permitem posicioná-lo e orientá-lo da maneira desejada. Essas configurações podem ser feitas diretamente na área de visualização (Viewport) por meio do respectivo menu ![menu viewport](./img/bottomMenu.PNG)
- **Shadow/Visible/Render Order:** São configurações exclusivas do editor (não geram alterações no output). Permitem controlar as sombras no modelo, a visibilidade e a ordem na cena.
