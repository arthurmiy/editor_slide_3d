# Slides

Todo o tour virtual é dividido em Slides. Muito mais do que um elemento utilizado para mostrar informações, o slide permite o controle preciso do posicionamento da câmera de acordo com o status de navegação.

Um slide possui a medida exata do tamanho da tela, sendo assim, o tamanho do slide será menor em telas menores.

## Elementos do slide

Um Slide é composto por dois elementos principais:

- Conteúdo
- Posição da câmera

---

### Conteúdo

O conteúdo de um slide é o texto, imagem e qualquer outro elemento HTML que for inserido dentro dele. A ferramenta permite, na tela de edição de slide, a pré-visualização do conteúdo.

![layouts](./img/slide_content.PNG)

De forma a facilitar o alinhamento na inserção de conteúdo a ferramenta disponibiliza algumas opções de layout pré-definidas.

![layouts](./img/slide_layout.PNG)

- **Título**: Exibe o texto inserido na forma de título em uma fonte grande centralizada. O plano de fundo fica em degradê começando em preto e terminando em transparente
-  **Cartão à esquerda/direita**: Cria um cartão alinhado ao lado esquerdo/direito da tela facilitando a organização do conteúdo
-  **Seleção nula**: Caso nenhuma das opções sejam selecionadas o conteúdo será apresentado da maneira que for inserida na caixa de texto (sem aplicação de nenhum estilo CSS).

---

### Posição da câmera

A posição da câmera define o cenário que vai estar sendo exibido como plano de fundo no slide. Esse posicionamento que controla toda a movimentação de câmera dentro do modelo tridimensional.

A posição da câmera é definida automaticamente no momento de criação do slide de acordo com a posição no momento em que o botão "NOVO" é pressionado.

Uma forma de editar a posição da câmera é reposicionar a visualização de acordo com a sua preferência e pressionar o botão "CAPTAR POSE". Caso a captura seja bem-sucedida aparecerá uma mensagem de confirmação.

Uma forma de visualizar a posição da câmera salva é pressionando o botão de edição. Quando a tela de edição é aberta ela reposiciona a câmera para a posição salva.

Caso o botão seja pressionado de maneira não intencional e a câmera acabe se movimentando de maneira não desejada, é possível reverter a movimentação a partir do comando de desfazer (undo) cujo atalhos é ctrl+z no teclado. Essa ação não é indicada para os casos nos quais foram feitas alterações no slide, afinal, ela acabará revertendo as mudanças antes de reverter o reposicionamento da câmera.

## Nome do slide
 
 É possível nomear o Slide no respectivo campo de texto. Esse nome serve apenas para identificação do slide, para servir de referência no momento de edição.

![slide](./img/slide.PNG)

## Ação de Entrada

O slide permite a seleção de uma [chave de animação](animation_key.md) a ser ativada no momento de entrada no slide. Ao selecionar essa chave de animação o elemento controlado pela chave será animado até ficar da forma esperada toda vez que o slide entrar na tela.

Ao sair do slide o objeto será animado para a chave de animação nomeada como default. Caso não haja uma animação com o nome 'Default' a primeira chave de animação será utilizada por padrão.

A lista de "Ações de entrada" é automaticamente atualizada toda vez que o usuário inclui novas chaves de animação. O padrão de exibição das chaves é *NOME_DO_OBJETO.NOME_DA_CHAVE*

![layouts](./img/slide_animation.PNG)