# Exportar projeto

A função de exportar o projeto permite que seja gerada a página web de visualização do tour virtual.

O formato de exportação é um arquivo .zip que possui a estrutura da página web e um executável que permite, em ambiente windows, visualizar de maneira simples e rápida o resultado.

O nome do arquivo zip é determinado pelo nome atribuido ao projeto na aba "project".

## Arquivos exportados

Dentro da pasta zip estão todos os elementos necessários para a visualização da página. A seguir está detalhado de maneira mais clara o conteúdo da pasta:

- **index.html:** página de visualização do tour, não é possível visualizá-la direto no brouser, é necessário prover a página a partir de um servidor apache, por exemplo.
- **start.exe:** executável para sistema operacional Windows que cria um servidor html que permite a visualização da página. Ao executar o arquivo a página é aberta automaticamente funiconando da forma esperada.
- **static:** Pasta contendo o conteúdo de código e assets utilizado na página de tour virtual
  - css: arquivos de estilo
  - customization: arquivos com configurações de animação, objetos, posição de câmera, etc
  - fonts: Arquivos de fonte utilizados no projeto
  - imgs: armazenamento de imagens (sem uso no momento)
  - java: arquivos com base do código da página do tour virtual
  - models: Contém os arquivos tridimensionais utilizados no projeto

## Arquivos não inclusos na exportação

De forma a diminuir a dimensão do arquivo zip final nem todo o conteúdo foi inserido no arquivo zip exportado. Para que o tour virtual funcione corretamente pode ser necessário incluir alguns arquivos manualmente.

De maneira resumida apenas os objetos tridimensionais importados deverão ser adicionados manualmente à pasta "models". Os objetos inseridos por meio do menu de inserção de objetos serão exportados automaticamente com o modelo.

É importante que os modelos adicionados à pasta models tenham o mesmo nome atribuido no ambiente de criação. De maneira geral, ao importar um arquivo, o respectivo objeto importado fica com o nome do arquivo original na árvore de objetos da cena, esse nome é utilizado no resultado final para a importação do arquivo.

---

**Exemplo**

Ao importar o arquivo "nome.glb" é inserido na cena do ambiente de criação um novo objeto criado a partir do arquivo cujo nome na árvore de objetos (menu [cena](interface.scene.md)). Ao exportar o arquivo o usuário deve incluir o arquivo "nome.glb" sem alterar nada no nome, dentro da pasta models.

Caso o nome do arquivo seja alterado, é possível atualizar o nome do objeto no ambiente de criação de forma que reflita o nome do arquivo que será posteriormente inserido na pastas de exportação.

---

## Execução no windows

Como dito anteriormente a execução no windows pode ser feita de maneira simples utilizando o executável incluido no zip exportado.

Ao executar o arquivo start.exe uma janela do prompt de comando do windows irá abrir (janela com fundo preto) e uma aba do navegador será iniciada. Enquanto a janela do prompt estiver aberta será possível acessar o tour utilizando o endereço "localhost:8080" no navegador. A partir do momento que o prompt for fechado, as execuções abertas permanecerão funcionando e novos acessos à página não será possível.

É importante salientar que, caso haja a necessidade de abrir outra apresentação, é necessário substituir os prompts em execução. De maneira geral o primeiro prompt aberto que irá ditar o que será visualizado no browser.
