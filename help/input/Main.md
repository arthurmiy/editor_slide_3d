# Introdução

Nessa página será documentada a ferramenta de edição de tours virtuais guiados. A ideia principal dessa ferramenta é permitir a criação de tours em locais modelados em 3D na forma de apresentação.

A ferramenta foi desenvolvida com base no editor 3D do threejs que pode ser acessado [aqui](https://threejs.org/editor/)

Nesse guia serão mostradas as ferramentas disponibilizadas no software e como utiilzá-las para criar um tour virtual.

# Conteúdo

## 1. Adicionando objetos na cena

- [Importando objetos externos](import.md)
- [Adicionando formas básicas](add.md)

## 2. Salvando, abrindo e exportando projetos

- [Salvando projetos](save.md)
- [Abrindo projetos](import.md)
- [Exportando projetos](export.md)

## 3. Interface gráfica

- [Propriedades de objetos](interface.object.md)
- [Propriedades de cena](interface.scene.md)
- [Área de visualização](viewport.md)

## 4. Slides

- [Criação e edição de slides](slides.md)

## 5. Ação e movimento

- [Scripts](scripts.md)
- [Chaves de animação](animation_key.md)
