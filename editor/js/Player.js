/**
 * @author mrdoob / http://mrdoob.com/
 */

import { UIPanel } from "./libs/ui.js";
import { APP } from "./libs/newapp.js";

function Player(editor) {
	var signals = editor.signals;

	var container = new UIPanel();
	container.setId("player");
	container.setPosition("absolute");

	container.setBackgroundColor("#272822");
	container.setDisplay("none");

	//

	var player = new APP.Player();
	container.dom.appendChild(player.dom);

	window.addEventListener("resize", function () {
		player.setSize(window.innerWidth, window.innerHeight);
	});

	signals.startPlayer.add(function () {
		container.setDisplay("");
		player.load(editor.toJSONPreview());
		player.setSize(window.innerWidth, window.innerHeight);
		player.play();
	});

	signals.stopPlayer.add(function () {
		container.setDisplay("none");
		player.stop();
		player.dispose();
	});

	return container;
}

export { Player };
