import { ENVController } from "../java/controllerModule.js";

//area  used  for control variables (e.g. visible/not visible, open/closed)

//area used to declare clickableObjs
var clickableObjs = ["towers", "chaves", "abrigo"];

//area used to list position of camera for each section (should give the chance to add +/- 2pi)
var positionsList = [
  {
    x: 3130.124598984848,
    y: 11864.42589490524,
    z: 5178.494277548814,
    rx: -1.2437893525466348,
    ry: 0.48840907237283754,
    rz: 0.9448886199537659,
  },
  {
    x: 2231.9004636363466,
    y: 754.108160420375,
    z: 4135.296735554023,
    rx: -0.21065373096971177,
    ry: 0.7825549212715945,
    rz: 0.14964026939254782,
  },
];

//section used for obj and text loading
var objectsToLoad = [
  {
    name: "se",
    children: [
      {
        name: "sala",
        path: "./models/divided/saladraco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "sinoticos",
        path: "./models/divided/sinoticosdraco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "setor1",
        path: "./models/divided/setor1draco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "setor2",
        path: "./models/divided/setor2draco.glb",
        scale: [100, 100, 100],
      },
      {
        name: "towers",
        path: "./models/divided/towersdraco.glb",
        scale: [100, 100, 100],
      },
    ],
  },

  {
    name: "aviso",
    path: "Elemento interativo.text",
    scale: [1, 1, 1],
    position: [{ x: -4250, y: 400, z: -1000 }],
    rotation: [0, -Math.PI / 3 - Math.PI / 2, 0],
    tProperties: {
      height: 20,
      size: 70,
      hover: 10,
      curveSegments: 4,
      bevelThickness: 10,
      bevelSize: 1.5,
      bevelEnabled: true,
      font: undefined,
      fontName: "optimer",
      fontWeight: "bold",
      color: 0x993333,
      mirror: true,
      mirrorOpacity: 0.2,
    },
    onLoadAnimation: {
      duration: 100,
      objName: "aviso",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0,
          target: "children",
          initialState: 1,
        },
        {
          property: "position",
          type: "absolute",
          value: [-4250, 0, -1000],
          target: "obj",
          initialState: [-4250, 400, -1000],
        },
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
          initialState: [0, -Math.PI / 3 - Math.PI / 2, 0],
        },
      ],
    },
  },
];

/////////////section used for animation keys storage

////////////Execution
var controller;

init();

function init() {
  controller = ENVController;
  controller.objectsToLoad = objectsToLoad;
  controller.clickableObjs = clickableObjs;
  controller.positionsList = positionsList;
  //controller.freeCamera

  controller.createEnvironment();
  //controller.getObjectByName (name)
  //controller.getValidParentName
  //controller.verifyParentName
  //controller/getOpacity
  //controller.getEmissiveIntensity
  //controller.setEmissive
  //controller.setOpacity
  //controller.animateKeys
  //controller.addTextToScene
  //controller.addGlbToScene
  //controller.addFbxToScene
  //controller.getCamCurrentPosition
  //controller.scrollStatus

  controller.animationKeys = animationKeys;
  controller.onObjClicked = onObjClicked;
  controller.onMouseOverObj = onObjOver;

  controller.onMouseOutOfObj = onObjOut;

  ///
}

//////callbacks

function onObjOver(obj) {
  if (controller.verifyParentName(obj, "chaves")) {
    controller.animateKeys([
      animationKeys.sec1.highlightOn,
      animationKeys.sec2.highlightOn,
    ]);
  }
}

function onObjOut(obj) {
  controller.animateKeys([
    animationKeys.sec1.highlightOff,
    animationKeys.sec2.highlightOff,
  ]);
}

function onObjClicked(obj) {
  if (controller.verifyParentName(obj, "chaves")) {
    if (opened) {
      controller.animateKeys([
        animationKeys.sec1.close,
        animationKeys.sec2.close,
      ]);
    } else {
      controller.animateKeys([
        animationKeys.sec1.open,
        animationKeys.sec2.open,
      ]);
    }
    opened = !opened;
  }
}
