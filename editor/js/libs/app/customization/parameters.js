import { ENVController } from "../java/controllerModule.js";

var positionsList = [<!-- positions -->];


var objectsToLoad = [<!-- objs -->];

var animationKeys = {<!-- ak -->};

var controller;

init();

function init() {
	controller = ENVController;
	controller.positionsList = positionsList;
	controller.objectsToLoad = objectsToLoad;

	controller.createEnvironment();


	controller.onSlideChange = slideChangeCallback;
	controller.onSlidePositionUpdate=onSlidePositionUpdate;
	controller.onObjClicked=onObjClicked;
	controller.onMouseOverObj=onMouseOverObj;
	controller.onMouseOutOfObj=onMouseOutObj;
}


var activeKeys = {};
var targetKeys = {};

function slideChangeCallback(oldSlide, newSlide) {

	targetKeys = {};

  //set target based on slide
  //targetKeys//

 //only changes to default if no future keys are added
  //defaultKeys//

  var finalAnimations=[];
  for (var i in targetKeys) {
	if (targetKeys[i]!=activeKeys[i]){
		finalAnimations.push(animationKeys[i][targetKeys[i]]);
	}
  }

  playSlideAnimation(finalAnimations);
  onSlideChange(oldSlide,newSlide);
}

function checkID (object, id) {
	return controller.verifyParentName(object,id);
}

function playSlideAnimation (listAnim) {
	if (listAnim.length!=[]) {
		controller.animateKeys(listAnim);
	}
}

<!-- code -->
