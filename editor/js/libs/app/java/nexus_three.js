import * as THREE from "./three.module.js";

var NexusObject = (  function() {
  function NexusObject (url, renderer, render, material) {
        var gl = renderer.getContext();
        var geometry = new THREE.BufferGeometry();
        var positions = new Float32Array(3);

        geometry.setAttribute(
          "position",
          new THREE.BufferAttribute(positions, 3)
        );

        if (!material) this.autoMaterial = true;

        THREE.Mesh.call(this, geometry, material);
        this.frustumCulled = false;

        var mesh = this;
        var instance = (this.instance = new Nexus.Instance(gl));
        instance.open(url);
        instance.onLoad = function () {
          var s = 1 / instance.mesh.sphere.radius;
          var pos = instance.mesh.sphere.center;
          mesh.position.set(-pos[0] * s, -pos[1] * s, -pos[2] * s);
          mesh.scale.set(s, s, s);
          if (mesh.autoMaterial)
            mesh.material = new THREE.MeshLambertMaterial({ color: 0xffffff });

          if (this.mesh.vertex.normal) {
            var normals = new Float32Array(3);
            geometry.setAttribute(
              "normal",
              new THREE.BufferAttribute(normals, 3)
            );
          }
          if (this.mesh.vertex.color) {
            var colors = new Float32Array(4);
            geometry.setAttribute(
              "color",
              new THREE.BufferAttribute(colors, 4)
            );
            if (mesh.autoMaterial)
              mesh.material = new THREE.MeshLambertMaterial({
                vertexColors: THREE.VertexColors,
              });
          }

          if (this.mesh.vertex.texCoord) {
            var uv = new Float32Array(2);
            geometry.setAttribute("uv", new THREE.BufferAttribute(uv, 2));
            if (mesh.autoMaterial) {
              var texture = new THREE.DataTexture(
                new Uint8Array([1, 1, 1]),
                1,
                1,
                THREE.RGBFormat
              );
              texture.needsUpdate = true;
              mesh.material = new THREE.MeshLambertMaterial({
                color: 0xffffff,
                map: texture,
              });
            }
          }

          if (this.mesh.face.index) {
            var indices = new Uint32Array(3);
            geometry.setIndex(new THREE.BufferAttribute(indices, 3));
          }
          render();
        };
        instance.onUpdate = function () {
          render();
        };

        this.onAfterRender = function (
          renderer,
          scene,
          camera,
          geometry,
          material,
          group
        ) {
          if (!instance.isReady) return;
          var s = new THREE.Vector2();
          renderer.getSize(s);
          instance.updateView(
            [0, 0, s.width, s.height],
            camera.projectionMatrix.elements,
            mesh.modelViewMatrix.elements
          );
          var program = renderer.getContext().getParameter(gl.CURRENT_PROGRAM);
          instance.attributes[
            "position"
          ] = renderer.getContext().getAttribLocation(program, "position");
          instance.attributes[
            "normal"
          ] = renderer.getContext().getAttribLocation(program, "normal");
          instance.attributes[
            "color"
          ] = renderer.getContext().getAttribLocation(program, "color");
          instance.attributes["uv"] = renderer
            .getContext()
            .getAttribLocation(program, "uv");

          instance.render();
        };
      }

      NexusObject.prototype = Object.assign( Object.create(THREE.Mesh.prototype),{
        constructor: NexusObject,
      });

      return NexusObject;
    }) ();

export { NexusObject };
