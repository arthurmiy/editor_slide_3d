document.getElementById("content").onscroll = () => {
  let scrollInfos = scrollStatus();
  let tmp = document.getElementsByClassName("fullscrn");

  if (scrollInfos.index + 1 < tmp.length) {
    tmp[scrollInfos.index + 1].children[0].style.opacity =
      scrollInfos.percent / 100;
    tmp[scrollInfos.index].children[0].style.opacity =
      (100 - scrollInfos.percent) / 100;
  }
};

function scrollStatus() {
  let doc = document.getElementById("content");
  let part = Math.floor(doc.scrollTop / doc.clientHeight);
  let tolerance = 30;
  let percentage =
    (100 * (doc.scrollTop - part * doc.clientHeight)) / doc.clientHeight;
  if (percentage < tolerance) {
    percentage = 0;
  } else {
    percentage = (100 * (percentage - tolerance)) / (100 - tolerance);
  }
  return {
    index: part,
    percent: percentage,
  };
}
