/**
 * @author mrdoob / http://mrdoob.com/
 */

var APP = {
	Player: function () {
		var dom = document.createElement("iframe");
		dom.setAttribute("id", "ifrm"); // assign an id

		this.dom = dom;

		this.load = function (json) {
			var doc;
			try {
				json.scene.object.background = 0;
			} catch (error) {}

			if (dom.contentDocument) {
				doc = dom.contentDocument;
			} else {
				doc = dom.contentWindow.document;
			}
			window.onkeydown = function (event) {
				var signals = editor.signals;
				switch (event.key.toLowerCase()) {
					case "backspace":
						event.preventDefault(); // prevent browser back

					// fall-through

					case "escape":
						if (window.isPlaying === true) {
							window.isPlaying = false;
							signals.stopPlayer.dispatch();
						}
						break;
				}
			};

			var content =
				`<!DOCTYPE html>
<html lang="en">

<head>
  <title>projeto</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
  <link rel="stylesheet" href="./js/libs/app/css/parent.css" />
  <link rel="icon" href="http://example.com/favicon.png">

</head>

<body>
  <div id="content" class="content" >


			/* content */



  </div>

    <script src="./js/libs/app/java/tween.js"></script>

  <script type="module"  >


import { ENVController } from "./js/libs/controllerModule.js";

var positionsList = [<!-- positions -->];


var animationKeys = {<!-- ak -->};

var controller;

init();

function init() {
	controller = ENVController;
	controller.positionsList = positionsList;


	controller.createEnvironment();

try {
		controller.onSlideChange = slideChangeCallback;
	} catch (error) {
			console.log(error)
		}

	try {
		controller.onSlidePositionUpdate=onSlidePositionUpdate;
	} catch (error) {
			console.log(error)
		}
	try {
		controller.onObjClicked=onObjClicked;
	} catch (error) {
			console.log(error)
		}
	try {
		controller.onMouseOverObj=onMouseOverObj;
	} catch (error) {
			console.log(error)
		}

	try {
		controller.onMouseOutOfObj=onMouseOutObj;
	} catch (error) {
			console.log(error)
		}


	controller.loadPreviewJson(\`` +
				JSON.stringify(json.scene) +
				`\`,"scene")
}


var activeKeys = {};
var targetKeys = {};

document.addEventListener(
		"keydown", window.parent.window.onkeydown,false);

function slideChangeCallback(oldSlide, newSlide) {

	targetKeys = {};

   //set target based on slide
  //targetKeys//

 //only changes to default if no future keys are added
  //defaultKeys//


  var finalAnimations=[];
  for (var i in targetKeys) {
	if (targetKeys[i]!=activeKeys[i]){
		finalAnimations.push(animationKeys[i][targetKeys[i]]);
	}
  }

  playSlideAnimation(finalAnimations);

try {
		onSlideChange(oldSlide,newSlide);
	} catch (error) {
			console.log(error)
		}

}

function checkID (object, id) {
	return controller.verifyParentName(object,id);
}

function playSlideAnimation (listAnim) {
	if (listAnim.length!=[]) {
		controller.animateKeys(listAnim);
	}
}




<!-- code -->



    </script>

    <script type="module" src="./js/libs/app/java/pagecontrollerHtml.js"></script>
</body>

</html>`;

			/////////////////////////////////
			var positionsList = "";
			var animationKeysList = "";
			var objs = "";
			var previousAngleX = null;
			var previousAngleY = null;
			var previousAngleZ = null;

			//construct positions list
			var slides = json.slides;

			var animationPerSlide = [];

			if (slides !== undefined && slides.length > 0) {
				for (var i = 0; i < slides.length; i++) {
					let slide = slides[i];
					positionsList =
						positionsList +
						"{x:" +
						slide.x +
						",y:" +
						slide.y +
						",z:" +
						slide.z +
						",rx:" +
						minAngle(slide.rx, previousAngleX) +
						",ry:" +
						minAngle(slide.ry, previousAngleY) +
						",rz:" +
						minAngle(slide.rz, previousAngleZ) +
						",},";

					previousAngleX = minAngle(slide.rx, previousAngleX);
					previousAngleY = minAngle(slide.ry, previousAngleY);
					previousAngleZ = minAngle(slide.rz, previousAngleZ);

					//aproveita loop para preencher lista de inActions
					if (slide.inAction) {
						animationPerSlide.push(slide.inAction);
					} else {
						animationPerSlide.push("");
					}
				}
				content = content.replace("<!-- positions -->", positionsList);
			} else {
				content = content.replace("<!-- positions -->", "");
			}

			///////////////////////////construct animations keys list
			var aks = json.animationKeys; //all animation keys
			var akString = ""; //final string to be included

			if (aks !== undefined && Object.keys(aks).length > 0) {
				for (var key in aks) {
					//look at each object in aks (stores object id in key)
					let aksObj = aks[key]; //stores all keys from a single obj
					//initialize object element with animation key (temporary because may not be added)
					let animationKeysGroup = "";

					for (var akID in aksObj) {
						let ak = aksObj[akID];
						//look each animationKey in a single obj
						let poseCounter = 0;

						let animationKeysTmp =
							ak.name + ': { objName:"' + key + '",poseKeys:[';

						if (ak.selected["rotation"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"rotation", type:"absolute", target:"obj", value:[' +
								ak.rotation[0] * THREE.MathUtils.DEG2RAD +
								"," +
								ak.rotation[1] * THREE.MathUtils.DEG2RAD +
								"," +
								ak.rotation[2] * THREE.MathUtils.DEG2RAD +
								"],},";
						}
						if (ak.selected["position"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"position", type:"absolute", target:"obj", value:[' +
								ak.position[0] +
								"," +
								ak.position[1] +
								"," +
								ak.position[2] +
								"],},";
						}
						if (ak.selected["scale"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"scale", type:"absolute", target:"obj", value:[' +
								ak.scale[0] +
								"," +
								ak.scale[1] +
								"," +
								ak.scale[2] +
								"],},";
						}
						if (ak.selected["opacity"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"Opacity", type:"absolute", target:"obj", value:' +
								ak.opacity +
								",},";
						}
						if (ak.selected["highlight"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"EmissiveIntensity", type:"absolute", target:"obj", value:' +
								ak.highlight +
								",},";
						}

						if (poseCounter > 0) {
							//included some stuff
							animationKeysTmp = animationKeysTmp + "],},";
						} else {
							animationKeysTmp = "";
						}

						animationKeysGroup = animationKeysGroup + animationKeysTmp; //if doesnt have keys save add nothing
					}

					//adds object string if the object has valid keys
					if (animationKeysGroup != "") {
						//has valid keys
						akString =
							akString + '"' + key + '": {' + animationKeysGroup + "},";
					}
				}
				content = content.replace("<!-- ak -->", akString);
			} else {
				content = content.replace("<!-- ak -->", "");
			}

			var targetKeysString = "";
			var defaultKeysString = "";

			//todo set default on beginning

			for (var index in animationPerSlide) {
				if (animationPerSlide[index] != "") {
					var animationSplit = animationPerSlide[index].split("@"); //animation name in [0] keyName in [1]
					var objKeyAnimations = json.animationKeys[animationSplit[0]];
					var defaultAnim;
					if (objKeyAnimations) {
						for (let keyA in objKeyAnimations) {
							if (objKeyAnimations[keyA].name == "default") {
								defaultAnim = "default";
							}
						}
						if (!defaultAnim) {
							defaultAnim = objKeyAnimations[0].name;
						}
					}

					targetKeysString =
						targetKeysString +
						"\nif (newSlide == " +
						index +
						') {\n targetKeys["' +
						animationSplit[0] +
						'"]="' +
						animationSplit[1] +
						'";}';
					defaultKeysString =
						defaultKeysString +
						"\nif (oldSlide == " +
						index +
						' && !targetKeys["' +
						animationSplit[0] +
						'"] ) {\n targetKeys["' +
						animationSplit[0] +
						'"]="' +
						defaultAnim +
						'";}';
				}
			}

			content = content.replace("//targetKeys//", targetKeysString);
			content = content.replace("//defaultKeys//", defaultKeysString);

			content = content.replace("<!-- code -->", json.scripts);

			var slides = json.slides;

			var slidesHtml = "";

			if (slides !== undefined && slides.length > 0) {
				for (var i = 0; i < slides.length; i++) {
					let parentDiv = document.createElement("div");
					let div = document.createElement("div");
					if (slides[i].type == "title") {
						parentDiv.classList.add("fullscrn");
						div.classList.add("title");
					} else if (slides[i].type == "left") {
						parentDiv.classList.add("area");

						div.classList.add("leftAll");
					} else if (slides[i].type == "right") {
						parentDiv.classList.add("area");

						div.classList.add("rightAll");
					} else if (slides[i].type == "none") {
						parentDiv.classList.add("fullscrn");
					}
					div.innerHTML = slides[i].source;

					parentDiv.appendChild(div);
					if (slides[i].type == "left" || slides[i].type == "right") {
						let tmpDiv = document.createElement("div");
						tmpDiv.classList.add("fullscrn");
						tmpDiv.appendChild(parentDiv);
						slidesHtml = slidesHtml + nodeToString(tmpDiv);
					} else {
						slidesHtml = slidesHtml + nodeToString(parentDiv);
					}
				}
			}

			content = content.replace("\n\t\t\t/* content */\n", slidesHtml);
			/////////////////////////////////

			dom.srcdoc = content;

			// renderer.shadowMap.enabled = project.shadows === true;
			// renderer.xr.enabled = project.vr === true;

			// this.setScene(loader.parse(json.scene));
			// this.setCamera(loader.parse(json.camera));

			// events = {
			// 	init: [],
			// 	start: [],
			// 	stop: [],
			// 	keydown: [],
			// 	keyup: [],
			// 	mousedown: [],
			// 	mouseup: [],
			// 	mousemove: [],
			// 	touchstart: [],
			// 	touchend: [],
			// 	touchmove: [],
			// 	update: [],
			// };

			// var scriptWrapParams = "player,renderer,scene,camera";
			// var scriptWrapResultObj = {};

			// for (var eventKey in events) {
			// 	scriptWrapParams += "," + eventKey;
			// 	scriptWrapResultObj[eventKey] = eventKey;
			// }

			// var scriptWrapResult = JSON.stringify(scriptWrapResultObj).replace(
			// 	/\"/g,
			// 	""
			// );

			// for (var uuid in json.scripts) {
			// 	var object = scene.getObjectByProperty("uuid", uuid, true);

			// 	if (object === undefined) {
			// 		console.warn("APP.Player: Script without object.", uuid);
			// 		continue;
			// 	}

			// 	var scripts = json.scripts[uuid];

			// 	for (var i = 0; i < scripts.length; i++) {
			// 		var script = scripts[i];

			// 		var functions = new Function(
			// 			scriptWrapParams,
			// 			script.source + "\nreturn " + scriptWrapResult + ";"
			// 		).bind(object)(this, renderer, scene, camera);

			// 		for (var name in functions) {
			// 			if (functions[name] === undefined) continue;

			// 			if (events[name] === undefined) {
			// 				console.warn("APP.Player: Event type not supported (", name, ")");
			// 				continue;
			// 			}

			// 			events[name].push(functions[name].bind(object));
			// 		}
			// 	}
			// }

			// dispatch(events.init, arguments);
		};

		this.setCamera = function (value) {
			camera = value;
			camera.aspect = this.width / this.height;
			camera.updateProjectionMatrix();
		};

		this.setScene = function (value) {
			scene = value;
		};

		this.setSize = function (width, height) {
			this.width = width;
			this.height = height;

			// if (camera) {
			// 	camera.aspect = this.width / this.height;
			// 	camera.updateProjectionMatrix();
			// }

			// if (renderer) {
			// 	renderer.setSize(width, height);
			// }
			dom.height = height;
			dom.width = width;
		};

		function dispatch(array, event) {
			for (var i = 0, l = array.length; i < l; i++) {
				array[i](event);
			}
		}

		function minAngle(angle, previous) {
			if (previous != null) {
				if (previous - angle > Math.PI) {
					return minAngle(angle + 2 * Math.PI, previous);
				} else if (previous - angle < -Math.PI) {
					return minAngle(angle - 2 * Math.PI, previous);
				} else {
					return angle;
				}
			} else {
				return angle;
			}
		}

		function nodeToString(node) {
			var tmpNode = document.createElement("div");
			tmpNode.appendChild(node.cloneNode(true));
			var str = tmpNode.innerHTML;
			tmpNode = node = null; // prevent memory leaks in IE
			return str;
		}

		var time, prevTime;

		function animate() {
			time = performance.now();

			try {
				dispatch(events.update, { time: time, delta: time - prevTime });
			} catch (e) {
				console.error(e.message || e, e.stack || "");
			}

			renderer.render(scene, camera);

			prevTime = time;
		}

		this.play = function () {
			// if (renderer.xr.enabled) dom.append(vrButton);
			// prevTime = performance.now();
			// document.addEventListener("keydown", onDocumentKeyDown);
			// document.addEventListener("keyup", onDocumentKeyUp);
			// document.addEventListener("mousedown", onDocumentMouseDown);
			// document.addEventListener("mouseup", onDocumentMouseUp);
			// document.addEventListener("mousemove", onDocumentMouseMove);
			// document.addEventListener("touchstart", onDocumentTouchStart);
			// document.addEventListener("touchend", onDocumentTouchEnd);
			// document.addEventListener("touchmove", onDocumentTouchMove);
			// dispatch(events.start, arguments);
			// renderer.setAnimationLoop(animate);
		};

		this.stop = function () {
			// if (renderer.xr.enabled) vrButton.remove();
			// document.removeEventListener("keydown", onDocumentKeyDown);
			// document.removeEventListener("keyup", onDocumentKeyUp);
			// document.removeEventListener("mousedown", onDocumentMouseDown);
			// document.removeEventListener("mouseup", onDocumentMouseUp);
			// document.removeEventListener("mousemove", onDocumentMouseMove);
			// document.removeEventListener("touchstart", onDocumentTouchStart);
			// document.removeEventListener("touchend", onDocumentTouchEnd);
			// document.removeEventListener("touchmove", onDocumentTouchMove);
			// dispatch(events.stop, arguments);
			// renderer.setAnimationLoop(null);
		};

		this.dispose = function () {
			// renderer.dispose();
			// camera = undefined;
			// scene = undefined;
		};

		//

		function onDocumentKeyDown(event) {
			dispatch(events.keydown, event);
		}

		function onDocumentKeyUp(event) {
			dispatch(events.keyup, event);
		}

		function onDocumentMouseDown(event) {
			dispatch(events.mousedown, event);
		}

		function onDocumentMouseUp(event) {
			dispatch(events.mouseup, event);
		}

		function onDocumentMouseMove(event) {
			dispatch(events.mousemove, event);
		}

		function onDocumentTouchStart(event) {
			dispatch(events.touchstart, event);
		}

		function onDocumentTouchEnd(event) {
			dispatch(events.touchend, event);
		}

		function onDocumentTouchMove(event) {
			dispatch(events.touchmove, event);
		}
	},
};

export { APP };
