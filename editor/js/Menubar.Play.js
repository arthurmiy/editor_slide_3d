/**
 * @author mrdoob / http://mrdoob.com/
 */

import { UIPanel } from "./libs/ui.js";

var MenubarPlay = function (editor) {
	var signals = editor.signals;
	var strings = editor.strings;

	var container = new UIPanel();
	container.setClass("menu");

	window.isPlaying = false;

	var title = new UIPanel();
	title.setClass("title");
	title.setTextContent(strings.getKey("menubar/play"));
	title.onClick(function () {
		if (window.isPlaying === false) {
			window.isPlaying = true;
			// title.setTextContent(strings.getKey("menubar/play/stop"));
			signals.startPlayer.dispatch();
		} else {
			window.isPlaying = false;
			// title.setTextContent(strings.getKey("menubar/play/play"));
			signals.stopPlayer.dispatch();
		}
	});
	container.add(title);

	return container;
};

export { MenubarPlay };
