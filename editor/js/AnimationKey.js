/**
 * @author mrdoob / http://mrdoob.com/
 */

import {
	UIElement,
	UIPanel,
	UIText,
	UIRow,
	UINumber,
	UICheckbox,
	UISelect,
	UITextArea,
	UIDiv,
} from "./libs/ui.js";

import { SetPositionCommand } from "./commands/SetPositionCommand.js";
import { SetAnimationKeyValueCommand } from "./commands/SetAnimationKeyValueCommand.js";
import { SetRotationCommand } from "./commands/SetRotationCommand.js";
import { SetScaleCommand } from "./commands/SetScaleCommand.js";

var AnimationKey = function (editor) {
	var signals = editor.signals;
	var block = false;

	var container = new UIPanel();
	container.setId("animationKey");
	container.setPosition("absolute");
	container.setBackgroundColor("#272822");
	container.setDisplay("none");

	var strings = editor.strings;

	var header = new UIPanel();
	header.setPadding("10px");
	container.add(header);

	var title = new UIText().setColor("#fff");
	header.add(title);

	var buttonSVG = (function () {
		var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		svg.setAttribute("width", 32);
		svg.setAttribute("height", 32);
		var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
		path.setAttribute("d", "M 12,12 L 22,22 M 22,12 12,22");
		path.setAttribute("stroke", "#fff");
		svg.appendChild(path);
		return svg;
	})();

	var close = new UIElement(buttonSVG);
	close.setPosition("absolute");
	close.setTop("3px");
	close.setRight("1px");
	close.setCursor("pointer");
	close.onClick(function () {
		container.setDisplay("none");
	});
	header.add(close);

	var renderer;

	signals.rendererChanged.add(function (newRenderer) {
		renderer = newRenderer;
	});

	var currentAnimationKey;

	///////////////////////
	var uidiv = new UIDiv();

	///////////////////// body
	var body = new UIPanel();
	body.setPadding("10px");
	container.add(body);

	var objectTypeRow = new UIRow();

	//Title
	objectTypeRow.add(
		new UIText(strings.getKey("slide/key_parameters"))
			.setWidth("200px")
			.setColor("#fff")
			.setFontSize("20px")
	);
	objectTypeRow.setPadding("10px");
	body.add(objectTypeRow);

	// position

	var objectPositionRow = new UIRow();
	var objectPositionX = new UINumber()
		.setPrecision(3)
		.setWidth("50px")
		.onChange(!block ? update : () => {});
	var objectPositionY = new UINumber()
		.setPrecision(3)
		.setWidth("50px")
		.onChange(!block ? update : () => {});
	var objectPositionZ = new UINumber()
		.setPrecision(3)
		.setWidth("50px")
		.onChange(!block ? update : () => {});

	var positionCheck = new UICheckbox()
		.onChange(!block ? update : () => {})
		.setLeft("75px");
	objectPositionRow.add(positionCheck);
	objectPositionRow.add(
		new UIText(strings.getKey("sidebar/object/position")).setWidth("90px")
	);
	objectPositionRow.add(objectPositionX, objectPositionY, objectPositionZ);

	container.add(objectPositionRow);

	// rotation

	var objectRotationRow = new UIRow();
	var objectRotationX = new UINumber()
		.setStep(10)
		.setNudge(0.1)
		.setUnit("°")
		.setWidth("50px")
		.onChange(!block ? update : () => {});
	var objectRotationY = new UINumber()
		.setStep(10)
		.setNudge(0.1)
		.setUnit("°")
		.setWidth("50px")
		.onChange(!block ? update : () => {});
	var objectRotationZ = new UINumber()
		.setStep(10)
		.setNudge(0.1)
		.setUnit("°")
		.setWidth("50px")
		.onChange(!block ? update : () => {});

	var rotationCheck = new UICheckbox()
		.onChange(!block ? update : () => {})
		.setLeft("75px");
	objectRotationRow.add(rotationCheck);
	objectRotationRow.add(
		new UIText(strings.getKey("sidebar/object/rotation")).setWidth("90px")
	);
	objectRotationRow.add(objectRotationX, objectRotationY, objectRotationZ);

	container.add(objectRotationRow);

	// scale

	var objectScaleRow = new UIRow();
	var objectScaleLock = new UICheckbox(true)
		.setPosition("absolute")
		.setLeft("75px");
	var objectScaleX = new UINumber(1)
		.setPrecision(3)
		.setRange(0.001, Infinity)
		.setWidth("50px")
		.onChange(updateScaleX);
	var objectScaleY = new UINumber(1)
		.setPrecision(3)
		.setRange(0.001, Infinity)
		.setWidth("50px")
		.onChange(updateScaleY);
	var objectScaleZ = new UINumber(1)
		.setPrecision(3)
		.setRange(0.001, Infinity)
		.setWidth("50px")
		.onChange(updateScaleZ);

	var scaleCheck = new UICheckbox()
		.onChange(!block ? update : () => {})
		.setLeft("75px");
	objectScaleRow.add(scaleCheck);
	objectScaleRow.add(
		new UIText(strings.getKey("sidebar/object/scale")).setWidth("90px")
	);
	objectScaleRow.add(objectScaleLock);
	objectScaleRow.add(objectScaleX, objectScaleY, objectScaleZ);

	container.add(objectScaleRow);

	// Opacity

	var objectOpacityRow = new UIRow();
	var objectOpacity = new UINumber(1)
		.setPrecision(3)
		.setRange(0.0, 1)
		.setWidth("50px")
		.onChange(!block ? update : () => {});

	var opacityCheck = new UICheckbox()
		.onChange(!block ? update : () => {})
		.setLeft("75px");
	objectOpacityRow.add(opacityCheck);
	objectOpacityRow.add(
		new UIText(strings.getKey("slide/opacity")).setWidth("90px")
	);

	objectOpacityRow.add(objectOpacity);

	container.add(objectOpacityRow);

	// Highlight

	var objectHighlightRow = new UIRow();
	var objectHighlight = new UINumber(1)
		.setPrecision(3)
		.setRange(0.0, 1)
		.setWidth("50px")
		.onChange(!block ? update : () => {});
	objectHighlight.setValue(0);

	var highlightCheck = new UICheckbox()
		.onChange(!block ? update : () => {})
		.setLeft("75px");
	objectHighlightRow.add(highlightCheck);
	objectHighlightRow.add(
		new UIText(strings.getKey("slide/highlight")).setWidth("90px")
	);

	objectHighlightRow.add(objectHighlight);

	container.add(objectHighlightRow);

	//

	signals.objectSelected.add(function (object) {
		container.setDisplay("none");
	});

	signals.objectChanged.add(function (object) {
		if (object !== editor.selected) return;

		updateUI(object);
	});

	// signals.refreshSidebarObject3D.add(function (object) {
	// 	if (object !== editor.selected) return;

	// 	updateUI(object);
	// });

	signals.editorCleared.add(function () {
		container.setDisplay("none");
	});

	signals.editAnimationKey.add(function (object, animationKey) {
		let name;
		currentAnimationKey = animationKey;
		if (typeof animationKey === "object") {
			name = animationKey.name;
			title.setValue("  " + name);
			updateFromAnimationKey(animationKey);
		}

		// currentMode = mode;

		// currentObject = object;

		container.setDisplay("");
	});

	signals.animationKeyRemoved.add(function (animationKey) {
		if (currentAnimationKey === animationKey) {
			container.setDisplay("none");
		}
	});

	return container;

	function updateFromAnimationKey(animationKey) {
		block = true;
		//position
		if (animationKey.selected.position) {
			objectPositionX.setValue(animationKey.position[0]);
			objectPositionY.setValue(animationKey.position[1]);
			objectPositionZ.setValue(animationKey.position[2]);
		}
		positionCheck.setValue(animationKey.selected.position);

		//rotation
		if (animationKey.selected.rotation) {
			objectRotationX.setValue(animationKey.rotation[0]);
			objectRotationY.setValue(animationKey.rotation[1]);
			objectRotationZ.setValue(animationKey.rotation[2]);
		}
		rotationCheck.setValue(animationKey.selected.rotation);

		//scale
		if (animationKey.selected.scale) {
			objectScaleX.setValue(animationKey.scale[0]);
			objectScaleY.setValue(animationKey.scale[1]);
			objectScaleZ.setValue(animationKey.scale[2]);
		}
		scaleCheck.setValue(animationKey.selected.scale);

		//opacity
		if (animationKey.selected.opacity) {
			objectOpacity.setValue(animationKey.opacity);
		}
		opacityCheck.setValue(animationKey.selected.opacity);

		//highlight
		if (animationKey.selected.highlight) {
			objectHighlight.setValue(animationKey.highlight);
		}
		highlightCheck.setValue(animationKey.selected.highlight);

		update();
		block = false;
	}

	function updateUI(object) {
		objectType.setValue(object.type);

		if (object.type == "Group") {
			expandRow.setDisplay("block");
		} else {
			expandRow.setDisplay("none");
		}

		objectUUID.setValue(object.uuid);
		objectName.setValue(object.name);

		objectPositionX.setValue(object.position.x);
		objectPositionY.setValue(object.position.y);
		objectPositionZ.setValue(object.position.z);

		objectRotationX.setValue(object.rotation.x * THREE.MathUtils.RAD2DEG);
		objectRotationY.setValue(object.rotation.y * THREE.MathUtils.RAD2DEG);
		objectRotationZ.setValue(object.rotation.z * THREE.MathUtils.RAD2DEG);

		objectScaleX.setValue(object.scale.x);
		objectScaleY.setValue(object.scale.y);
		objectScaleZ.setValue(object.scale.z);

		// try {

		// 	objectUserData.setValue( JSON.stringify( object.userData, null, '  ' ) );

		// } catch ( error ) {

		// 	console.log( error );

		// }

		// objectUserData.setBorderColor( 'transparent' );
		// objectUserData.setBackgroundColor( '' );

		// updateTransformRows(object);
	}

	function updateUI(object) {
		objectPositionX.setValue(object.position.x);
		objectPositionY.setValue(object.position.y);
		objectPositionZ.setValue(object.position.z);

		objectRotationX.setValue(object.rotation.x * THREE.MathUtils.RAD2DEG);
		objectRotationY.setValue(object.rotation.y * THREE.MathUtils.RAD2DEG);
		objectRotationZ.setValue(object.rotation.z * THREE.MathUtils.RAD2DEG);

		objectScaleX.setValue(object.scale.x);
		objectScaleY.setValue(object.scale.y);
		objectScaleZ.setValue(object.scale.z);
	}

	function updateScaleX() {
		var object = editor.selected;

		if (objectScaleLock.getValue() === true) {
			var scale = objectScaleX.getValue() / object.scale.x;

			objectScaleY.setValue(objectScaleY.getValue() * scale);
			objectScaleZ.setValue(objectScaleZ.getValue() * scale);
		}

		if (!block) {
			update();
		}
	}

	function updateScaleY() {
		var object = editor.selected;

		if (objectScaleLock.getValue() === true) {
			var scale = objectScaleY.getValue() / object.scale.y;

			objectScaleX.setValue(objectScaleX.getValue() * scale);
			objectScaleZ.setValue(objectScaleZ.getValue() * scale);
		}

		if (!block) {
			update();
		}
	}

	function updateScaleZ() {
		var object = editor.selected;

		if (objectScaleLock.getValue() === true) {
			var scale = objectScaleZ.getValue() / object.scale.z;

			objectScaleX.setValue(objectScaleX.getValue() * scale);
			objectScaleY.setValue(objectScaleY.getValue() * scale);
		}

		if (!block) {
			update();
		}
	}

	function update() {
		var object = editor.selected;
		let tmp = currentAnimationKey.selected;

		if (object !== null) {
			if (!block) {
				//// position Key
				if (positionCheck.getValue()) {
					//add position

					tmp.position = true;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"position",
							[
								objectPositionX.getValue(),
								objectPositionY.getValue(),
								objectPositionZ.getValue(),
							]
						)
					);
				} else {
					//remove postion
					tmp.position = false;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
				}

				//// rotation Key
				if (rotationCheck.getValue()) {
					//add rotation

					tmp.rotation = true;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"rotation",
							[
								objectRotationX.getValue(),
								objectRotationY.getValue(),
								objectRotationZ.getValue(),
							]
						)
					);
				} else {
					//remove postion
					tmp.rotation = false;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
				}

				//// scale Key
				if (scaleCheck.getValue()) {
					//add scale

					tmp.scale = true;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"scale",
							[
								objectScaleX.getValue(),
								objectScaleY.getValue(),
								objectScaleZ.getValue(),
							]
						)
					);
				} else {
					//remove postion
					tmp.scale = false;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
				}

				//// opacity Key
				if (opacityCheck.getValue()) {
					//add scale

					tmp.opacity = true;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"opacity",
							objectOpacity.getValue()
						)
					);
				} else {
					//remove opacity
					tmp.opacity = false;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
				}

				//// highlight Key
				if (highlightCheck.getValue()) {
					//add scale

					tmp.highlight = true;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"highlight",
							objectHighlight.getValue()
						)
					);
				} else {
					//remove highlight
					tmp.highlight = false;
					editor.execute(
						new SetAnimationKeyValueCommand(
							editor,
							object,
							currentAnimationKey,
							"selected",
							tmp
						)
					);
				}
			}

			/////////////////////////////// update positions

			var newRotation = new THREE.Euler(
				objectRotationX.getValue() * THREE.MathUtils.DEG2RAD,
				objectRotationY.getValue() * THREE.MathUtils.DEG2RAD,
				objectRotationZ.getValue() * THREE.MathUtils.DEG2RAD
			);
			if (
				object.rotation.toVector3().distanceTo(newRotation.toVector3()) >=
					0.01 &&
				rotationCheck.getValue()
			) {
				object.rotation.copy(newRotation);
				// updateMatrixWorld(true);
				// signals.objectChanged.dispatch(this.object);
			}

			var newPosition = new THREE.Vector3(
				objectPositionX.getValue(),
				objectPositionY.getValue(),
				objectPositionZ.getValue()
			);
			if (
				object.position.distanceTo(newPosition) >= 0.01 &&
				positionCheck.getValue()
			) {
				object.position.copy(newPosition);
				// editor.execute(new SetPositionCommand(editor, object, newPosition));
			}

			var newScale = new THREE.Vector3(
				objectScaleX.getValue(),
				objectScaleY.getValue(),
				objectScaleZ.getValue()
			);
			if (object.scale.distanceTo(newScale) >= 0.01 && scaleCheck.getValue()) {
				object.scale.copy(newScale);
				// editor.execute(new SetScaleCommand(editor, object, newScale));
			}

			object.updateMatrixWorld(true);
			signals.objectChanged.dispatch(object);

			// try {

			// 	var userData = JSON.parse( objectUserData.getValue() );
			// 	if ( JSON.stringify( object.userData ) != JSON.stringify( userData ) ) {

			// 		editor.execute( new SetValueCommand( editor, object, 'userData', userData ) );

			// 	}

			// } catch ( exception ) {

			// 	console.warn( exception );

			// }
		}
	}
};

export { AnimationKey };
