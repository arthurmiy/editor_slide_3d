/**
 * @author mrdoob / http://mrdoob.com/
 */

import {
	UIPanel,
	UIBreak,
	UIText,
	UIButton,
	UIRow,
	UIInput,
} from "./libs/ui.js";

import { AddSlideCommand } from "./commands/AddSlideCommand.js";
import { AddSlideToPositionCommand } from "./commands/AddSlideToPositionCommand.js";
import { SetSlideValueCommand } from "./commands/SetSlideValueCommand.js";
import { SetPositionCommand } from "./commands/SetPositionCommand.js";
import { SetRotationCommand } from "./commands/SetRotationCommand.js";
import { RemoveSlideCommand } from "./commands/RemoveSlideCommand.js";

var SidebarSlide = function (editor) {
	var strings = editor.strings;

	var signals = editor.signals;

	var container = new UIPanel();
	//container.setDisplay( 'none' );

	container.add(
		new UIText(strings.getKey("sidebar/slide")).setTextTransform("uppercase")
	);
	container.add(new UIBreak());
	container.add(new UIBreak());

	//

	var slidesContainer = new UIRow();
	container.add(slidesContainer);

	var newSlide = new UIButton(strings.getKey("sidebar/slide/new"));
	newSlide.onClick(function () {
		var slide = {
			name: "Slide " + (editor.slides.length + 1),
			source: "",
			type: "none",
			x: editor.viewportCamera.position.x,
			y: editor.viewportCamera.position.y,
			z: editor.viewportCamera.position.z,
			rx: editor.viewportCamera.rotation.x,
			ry: editor.viewportCamera.rotation.y,
			rz: editor.viewportCamera.rotation.z,
		};
		editor.execute(new AddSlideCommand(editor, slide));
	});
	container.add(newSlide);

	/*
	var loadScript = new UI.Button( 'Load' );
	loadScript.setMarginLeft( '4px' );
	container.add( loadScript );
	*/

	//

	function update() {
		slidesContainer.clear();
		// slidesContainer.setDisplay( 'none' );

		var slides = editor.slides;

		if (slides !== undefined && slides.length > 0) {
			slidesContainer.setDisplay("block");

			for (var i = 0; i < slides.length; i++) {
				(function (slide) {
					var name = new UIInput(slide.name)
						.setWidth("130px")
						.setFontSize("12px");
					name.onChange(function () {
						editor.execute(
							new SetSlideValueCommand(editor, slide, "name", this.getValue())
						);
					});
					slidesContainer.add(name);

					var edit = new UIButton(strings.getKey("sidebar/slide/edit"));
					edit.setMarginLeft("4px");
					edit.onClick(function () {
						var newPosition = new THREE.Vector3(slide.x, slide.y, slide.z);
						if (
							editor.viewportCamera.position.distanceTo(newPosition) >= 0.01
						) {
							editor.execute(
								new SetPositionCommand(
									editor,
									editor.viewportCamera,
									newPosition
								)
							);
						}

						var newRotation = new THREE.Euler(slide.rx, slide.ry, slide.rz);
						if (
							editor.viewportCamera.rotation
								.toVector3()
								.distanceTo(newRotation.toVector3()) >= 0.01
						) {
							editor.execute(
								new SetRotationCommand(
									editor,
									editor.viewportCamera,
									newRotation
								)
							);
						}

						signals.editSlide.dispatch(slide);
					});
					slidesContainer.add(edit);

					var pose = new UIButton(strings.getKey("slide/capture_pose"));
					pose.setMarginLeft("4px");
					pose.onClick(function () {
						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"x",
								editor.viewportCamera.position.x
							)
						);
						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"y",
								editor.viewportCamera.position.y
							)
						);
						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"z",
								editor.viewportCamera.position.z
							)
						);

						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"rx",
								editor.viewportCamera.rotation.x
							)
						);
						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"ry",
								editor.viewportCamera.rotation.y
							)
						);
						editor.execute(
							new SetSlideValueCommand(
								editor,
								slide,
								"rz",
								editor.viewportCamera.rotation.z
							)
						);

						alert(strings.getKey("msg/pose_captured") + slide.name);
					});
					slidesContainer.add(pose);

					var remove = new UIButton(strings.getKey("sidebar/slide/remove"));
					remove.setMarginLeft("4px");
					remove.onClick(function () {
						if (confirm(strings.getKey("msg/confirm"))) {
							editor.execute(new RemoveSlideCommand(editor, slide));
						}
					});
					slidesContainer.add(remove);

					var addSlide = new UIButton("+");
					addSlide.setMarginLeft("4px");
					addSlide.onClick(function () {
						var slide2 = {
							name: "Slide " + (editor.slides.length + 1),
							source: "",
							type: "none",
							x: editor.viewportCamera.position.x,
							y: editor.viewportCamera.position.y,
							z: editor.viewportCamera.position.z,
							rx: editor.viewportCamera.rotation.x,
							ry: editor.viewportCamera.rotation.y,
							rz: editor.viewportCamera.rotation.z,
						};
						editor.execute(
							new AddSlideToPositionCommand(
								editor,
								slide2,
								editor.slides.indexOf(slide)
							)
						);
					});
					slidesContainer.add(addSlide);

					var moveUpSlide = new UIButton("↑");
					//moveUpSlide.setMarginLeft("4px");
					moveUpSlide.onClick(function () {
						editor.execute(
							new AddSlideToPositionCommand(
								editor,
								slide,
								editor.slides.indexOf(slide) - 1
							)
						);
					});
					slidesContainer.add(moveUpSlide);

					var moveDownSlide = new UIButton("↓");
					//moveUpSlide.setMarginLeft("4px");
					moveDownSlide.onClick(function () {
						editor.execute(
							new AddSlideToPositionCommand(
								editor,
								slide,
								editor.slides.indexOf(slide) + 1
							)
						);
					});
					slidesContainer.add(moveDownSlide);

					slidesContainer.add(new UIBreak());
				})(slides[i]);
			}
		}
	}

	// signals

	// signals.objectSelected.add( function ( object ) {

	// 	if ( object !== null && editor.scene == object ) {

	// 		container.setDisplay( 'block' );

	// 		update();

	// 	} else {

	// 		container.setDisplay( 'none' );

	// 	}

	// } );

	signals.slideAdded.add(update);
	signals.slideRemoved.add(update);
	signals.slideChanged.add(update);
	signals.editorCleared.add(update);

	signals.sceneGraphChanged.add(update);

	return container;
};

export { SidebarSlide };
