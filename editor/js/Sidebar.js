/**
 * @author mrdoob / http://mrdoob.com/
 */

import { UITabbedPanel, UISpan } from "./libs/ui.js";

import { SidebarScene } from "./Sidebar.Scene.js";
import { SidebarProperties } from "./Sidebar.Properties.js";
import { SidebarScript } from "./Sidebar.Script.js";
import { SidebarAnimationKey } from "./Sidebar.AnimationKey.js";
import { SidebarSlide } from "./Sidebar.Slide.js";
import { SidebarAnimation } from "./Sidebar.Animation.js";
import { SidebarProject } from "./Sidebar.Project.js";
import { SidebarHistory } from "./Sidebar.History.js";
import { SidebarSettings } from "./Sidebar.Settings.js";

var Sidebar = function (editor) {
	var signals = editor.signals;
	var strings = editor.strings;

	var container = new UITabbedPanel();
	container.setId("sidebar");

	var scene = new UISpan().add(
		new SidebarScene(editor),
		new SidebarProperties(editor),
		new SidebarAnimation(editor),
		new SidebarScript(editor),
		new SidebarAnimationKey(editor),
		new SidebarSlide(editor)
	);

	var project = new SidebarProject(editor);

	var settings = new UISpan().add(
		new SidebarSettings(editor),
		new SidebarHistory(editor)
	);

	container.addTab("scene", strings.getKey("sidebar/scene"), scene);
	container.addTab("project", strings.getKey("sidebar/project"), project);
	container.addTab("settings", strings.getKey("sidebar/settings"), settings);
	container.select("scene");

	signals.startPlayer.add(function () {
		container.setDisplay("none");
	});

	signals.stopPlayer.add(function () {
		container.setDisplay("");
	});

	return container;
};

export { Sidebar };
