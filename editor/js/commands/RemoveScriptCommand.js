/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param script javascript object
 * @constructor
 */
var RemoveScriptCommand = function (editor, script) {
	Command.call(this, editor);

	this.type = "RemoveScriptCommand";
	this.name = "Remove Script";

	this.script = script;
};

RemoveScriptCommand.prototype = {
	execute: function () {
		if (this.editor.scripts === undefined) return;

		this.editor.scripts = this.editor.defaultScript;

		this.editor.signals.scriptRemoved.dispatch(this.script);
	},

	undo: function () {
		this.editor.scripts = this.script;

		this.editor.signals.scriptAdded.dispatch(this.script);
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.script = this.script;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.script = json.script;
	},
};

export { RemoveScriptCommand };
