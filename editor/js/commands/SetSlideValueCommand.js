/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from '../Command.js';

/**
 * @param editor Editor
 * @param slide javascript object
 * @param attributeName string
 * @param newValue string, object
 * @constructor
 */
var SetSlideValueCommand = function ( editor, slide, attributeName, newValue ) {

	Command.call( this, editor );

	this.type = 'SetSlideValueCommand';
	this.name = 'Set Slide.' + attributeName;
	this.updatable = true;

	this.slide = slide;

	this.attributeName = attributeName;
	this.oldValue = ( slide !== undefined ) ? slide[ this.attributeName ] : undefined;
	this.newValue = newValue;

};

SetSlideValueCommand.prototype = {

	execute: function () {

		this.slide[ this.attributeName ] = this.newValue;

		this.editor.signals.slideChanged.dispatch();

	},

	undo: function () {

		this.slide[ this.attributeName ] = this.oldValue;

		this.editor.signals.slideChanged.dispatch();

	},

	update: function ( cmd ) {

		this.newValue = cmd.newValue;

	},

	toJSON: function () {

		var output = Command.prototype.toJSON.call( this );

		output.index = this.editor.slides.indexOf( this.slide );
		output.attributeName = this.attributeName;
		output.oldValue = this.oldValue;
		output.newValue = this.newValue;

		return output;

	},

	fromJSON: function ( json ) {

		Command.prototype.fromJSON.call( this, json );

		this.oldValue = json.oldValue;
		this.newValue = json.newValue;
		this.attributeName = json.attributeName;
		this.slide = this.editor.slides[ json.index ];

	}

};

export { SetSlideValueCommand };
