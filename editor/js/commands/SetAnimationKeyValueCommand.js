/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param script javascript object
 * @param attributeName string
 * @param newValue string, object
 * @constructor
 */
var SetAnimationKeyValueCommand = function (
	editor,
	object,
	animationKey,
	attributeName,
	newValue
) {
	Command.call(this, editor);

	this.type = "SetAnimationKeyValueCommand";
	this.name = "Set animationKey." + attributeName;
	this.updatable = true;

	this.object = object;
	this.animationKey = animationKey;

	this.attributeName = attributeName;
	this.oldValue =
		animationKey !== undefined ? animationKey[this.attributeName] : undefined;
	this.newValue = newValue;
};

SetAnimationKeyValueCommand.prototype = {
	execute: function () {
		this.animationKey[this.attributeName] = this.newValue;

		this.editor.signals.animationKeyChanged.dispatch();
	},

	undo: function () {
		this.animationKey[this.attributeName] = this.oldValue;

		this.editor.signals.animationKeyChanged.dispatch();
	},

	update: function (cmd) {
		this.newValue = cmd.newValue;
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.objectUuid = this.object.uuid;
		output.index = this.editor.animationKeys[this.object.uuid].indexOf(
			this.animationKey
		);
		output.attributeName = this.attributeName;
		output.oldValue = this.oldValue;
		output.newValue = this.newValue;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.oldValue = json.oldValue;
		this.newValue = json.newValue;
		this.attributeName = json.attributeName;
		this.object = this.editor.objectByUuid(json.objectUuid);
		this.animationKey = this.editor.animationKeys[json.objectUuid][json.index];
	},
};

export { SetAnimationKeyValueCommand };
