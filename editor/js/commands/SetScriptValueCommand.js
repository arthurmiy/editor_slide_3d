/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param script javascript object
 * @param attributeName string
 * @param newValue string, object
 * @constructor
 */
var SetScriptValueCommand = function (editor, script, attributeName, newValue) {
	Command.call(this, editor);

	this.type = "SetScriptValueCommand";
	this.name = "Set Script." + attributeName;
	this.updatable = true;

	this.script = script;

	this.attributeName = attributeName;
	this.oldValue = script !== undefined ? script[this.attributeName] : undefined;
	this.newValue = newValue;
};

SetScriptValueCommand.prototype = {
	execute: function () {
		this.script[this.attributeName] = this.newValue;

		editor.scripts = this.newValue;

		this.editor.signals.scriptChanged.dispatch();
	},

	undo: function () {
		this.script[this.attributeName] = this.oldValue;

		editor.scripts = this.oldValue;

		this.editor.signals.scriptChanged.dispatch();
	},

	update: function (cmd) {
		this.newValue = cmd.newValue;
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.attributeName = this.attributeName;
		output.oldValue = this.oldValue;
		output.newValue = this.newValue;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.oldValue = json.oldValue;
		this.newValue = json.newValue;
		this.attributeName = json.attributeName;
		this.script = {
			name: "Custom Script",
			source: editor.scripts,
		};
	},
};

export { SetScriptValueCommand };
