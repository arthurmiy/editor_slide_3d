/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param script javascript object
 * @constructor
 */
var RemoveAnimationKeyCommand = function (editor, object, animationKey) {
	Command.call(this, editor);

	this.type = "RemoveAnimationKeyCommand";
	this.name = "Remove AnimationKey";

	this.object = object;
	this.animationKey = animationKey;
	if (this.object && this.animationKey) {
		this.index = this.editor.animationKeys[this.object.uuid].indexOf(
			this.animationKey
		);
	}
};

RemoveAnimationKeyCommand.prototype = {
	execute: function () {
		if (this.editor.animationKeys[this.object.uuid] === undefined) return;

		if (this.index !== -1) {
			this.editor.animationKeys[this.object.uuid].splice(this.index, 1);
		}

		this.editor.signals.animationKeyRemoved.dispatch(this.animationKey);
	},

	undo: function () {
		if (this.editor.animationKeys[this.object.uuid] === undefined) {
			this.editor.animationKeys[this.object.uuid] = [];
		}

		this.editor.animationKeys[this.object.uuid].splice(
			this.index,
			0,
			this.animationKey
		);

		this.editor.signals.animationKeyAdded.dispatch(this.animationKey);
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.objectUuid = this.object.uuid;
		output.animationKey = this.animationKey;
		output.index = this.index;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.animationKey = json.animationKey;
		this.index = json.index;
		this.object = this.editor.objectByUuid(json.objectUuid);
	},
};

export { RemoveAnimationKeyCommand };
