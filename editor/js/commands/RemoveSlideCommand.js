/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from '../Command.js';

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param slide javascript object
 * @constructor
 */
var RemoveSlideCommand = function ( editor, slide ) {

	Command.call( this, editor );

	this.type = 'RemoveSlideCommand';
	this.name = 'Remove Slide';

	this.slide = slide;
	if ( this.slide ) {

		this.index = this.editor.slides.indexOf( this.slide );

	}

};

RemoveSlideCommand.prototype = {

	execute: function () {


		if ( this.index !== - 1 ) {

			this.editor.slides.splice( this.index, 1 );

		}

		this.editor.signals.slideRemoved.dispatch( this.slide );

	},

	undo: function () {



		this.editor.slides.splice( this.index, 0, this.slide );

		this.editor.signals.slideAdded.dispatch( this.slide );

	},

	toJSON: function () {

		var output = Command.prototype.toJSON.call( this );

		output.slide = this.slide;
		output.index = this.index;

		return output;

	},

	fromJSON: function ( json ) {

		Command.prototype.fromJSON.call( this, json );

		this.slide = json.slide;
		this.index = json.index;
		this.object = this.editor.objectByUuid( json.objectUuid );

	}

};

export { RemoveSlideCommand };
