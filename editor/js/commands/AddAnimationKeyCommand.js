/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param script javascript object
 * @constructor
 */
var AddAnimationKeyCommand = function (editor, object, animationKey) {
	Command.call(this, editor);

	this.type = "AddAnimationKeyCommand";
	this.name = "Add AnimationKey";

	this.object = object;
	this.animationKey = animationKey;
};

AddAnimationKeyCommand.prototype = {
	execute: function () {
		if (this.editor.animationKeys[this.object.uuid] === undefined) {
			this.editor.animationKeys[this.object.uuid] = [];
		}

		this.editor.animationKeys[this.object.uuid].push(this.animationKey);

		this.editor.signals.animationKeyAdded.dispatch(this.animationKey);
	},

	undo: function () {
		if (this.editor.animationKeys[this.object.uuid] === undefined) return;

		var index = this.editor.animationKeys[this.object.uuid].indexOf(
			this.animationKey
		);

		if (index !== -1) {
			this.editor.animationKeys[this.object.uuid].splice(index, 1);
		}

		this.editor.signals.animationKeyRemoved.dispatch(this.animationKey);
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.objectUuid = this.object.uuid;
		output.animationKey = this.animationKey;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.animationKey = json.animationKey;
		this.object = this.editor.objectByUuid(json.objectUuid);
	},
};

export { AddAnimationKeyCommand };
