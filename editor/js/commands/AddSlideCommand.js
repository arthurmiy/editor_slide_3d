/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from '../Command.js';

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param slide javascript object
 * @constructor
 */
var AddSlideCommand = function ( editor, slide ) {

	Command.call( this, editor );

	this.type = 'AddSlideCommand';
	this.name = 'Add Slide';

	this.slide = slide;

};

AddSlideCommand.prototype = {

	execute: function () {

		this.editor.slides.push( this.slide );

		this.editor.signals.slideAdded.dispatch( this.slide );

	},

	undo: function () {

		var index = this.editor.slides.indexOf( this.slide );

		if ( index !== - 1 ) {

			this.editor.slides.splice( index, 1 );

		}

		this.editor.signals.slideRemoved.dispatch( this.slide );

	},

	toJSON: function () {

		var output = Command.prototype.toJSON.call( this );

		output.slide = this.slide;

		return output;

	},

	fromJSON: function ( json ) {

		Command.prototype.fromJSON.call( this, json );

		this.slide = json.slide;
		this.object = this.editor.objectByUuid( json.objectUuid );

	}

};

export { AddSlideCommand };
