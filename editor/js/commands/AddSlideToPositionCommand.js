/**
 * @author dforrer / https://github.com/dforrer
 * Developed as part of a project at University of Applied Sciences and Arts Northwestern Switzerland (www.fhnw.ch)
 */

import { Command } from "../Command.js";

/**
 * @param editor Editor
 * @param object THREE.Object3D
 * @param slide javascript object
 * @constructor
 */
var AddSlideToPositionCommand = function (editor, slide, position) {
	Command.call(this, editor);

	this.type = "AddSlideToPositionCommand";
	this.name = "Add Slide To Position";

	this.slide = slide;
	if (this.slide) {
		this.index = this.editor.slides.indexOf(this.slide);
		this.newPosition = position;
	}
};

AddSlideToPositionCommand.prototype = {
	execute: function () {
		if (this.index !== -1) {
			this.editor.slides.splice(this.index, 1);
			this.editor.signals.slideRemoved.dispatch(this.slide);
			this.editor.slides.splice(this.newPosition, 0, this.slide);
		} else {
			this.editor.slides.splice(this.newPosition, 0, this.slide);
		}

		this.editor.signals.slideAdded.dispatch(this.slide);
	},

	undo: function () {
		if (this.index !== -1) {
			this.editor.slides.splice(this.newPosition, 1);
			this.editor.signals.slideRemoved.dispatch(this.slide);
			this.editor.slides.splice(this.index, 0, this.slide);
			this.editor.signals.slideAdded.dispatch(this.slide);
		} else {
			this.editor.slides.splice(this.newPosition, 1);
			this.editor.signals.slideRemoved.dispatch(this.slide);
		}
	},

	toJSON: function () {
		var output = Command.prototype.toJSON.call(this);

		output.slide = this.slide;
		output.index = this.index;
		output.newPosition = this.newPosition;

		return output;
	},

	fromJSON: function (json) {
		Command.prototype.fromJSON.call(this, json);

		this.slide = json.slide;
		this.index = json.index;
		this.newPosition = json.newPosition;
		this.object = this.editor.objectByUuid(json.objectUuid);
	},
};

export { AddSlideToPositionCommand };
