/**
 * @author mrdoob / http://mrdoob.com/
 */

import * as THREE from "../../build/three.module.js";

import { UIRow, UIText, UINumber, UIInput } from "./libs/ui.js";

import { SetGeometryCommand } from "./commands/SetGeometryCommand.js";

var SidebarGeometryText = function (editor, object) {
	var strings = editor.strings;

	var container = new UIRow();

	var geometry = object.geometry;
	var parameters = geometry.parameters;

	// name

	var valueRow = new UIRow();
	var value = new UIInput()
		.setWidth("150px")
		.setFontSize("12px")
		.onChange(update);
	value.setValue(object.userData.value);

	valueRow.add(new UIText("Text")).setWidth("90px");
	valueRow.add(value);

	container.add(valueRow);

	// size

	var sizeRow = new UIRow();
	var size = new UINumber(object.userData.size).onChange(update);

	sizeRow.add(new UIText("Size").setWidth("90px"));
	sizeRow.add(size);

	container.add(sizeRow);

	// height

	var heightRow = new UIRow();
	var height = new UINumber(object.userData.height).onChange(update);

	heightRow.add(
		new UIText(strings.getKey("sidebar/geometry/box_geometry/height")).setWidth(
			"90px"
		)
	);
	heightRow.add(height);

	container.add(heightRow);

	// bevel thickness

	var bevelThickRow = new UIRow();
	var bevelThick = new UINumber(object.userData.bevelThickness).onChange(
		update
	);

	bevelThickRow.add(new UIText("Bevel thickness").setWidth("90px"));
	bevelThickRow.add(bevelThick);

	container.add(bevelThickRow);

	// bevel size

	bevel;

	var bevelRow = new UIRow();
	var bevel = new UINumber(object.userData.bevelSize).onChange(update);

	bevelRow.add(new UIText("Bevel size").setWidth("90px"));
	bevelRow.add(bevel);

	container.add(bevelRow);

	//

	function update() {
		let extracted = value.getValue();
		let font = editor.font;
		let textGeo = new THREE.TextGeometry(extracted, {
			font,

			size: size.getValue(),
			height: height.getValue(),
			curveSegments: 4,

			bevelThickness: bevelThick.getValue(),
			bevelSize: bevel.getValue(),
			bevelEnabled: true,
		});

		textGeo.computeBoundingBox();
		textGeo.computeVertexNormals();

		const triangle = new THREE.Triangle();

		// "fix" side normals by removing z-component of normals for side faces
		// (this doesn't work well for beveled geometry as then we lose nice curvature around z-axis)

		let bevelEnabled = true;

		if (!bevelEnabled) {
			const triangleAreaHeuristics = 0.1 * (height * size);

			for (let i = 0; i < textGeo.faces.length; i++) {
				const face = textGeo.faces[i];

				if (face.materialIndex == 1) {
					for (let j = 0; j < face.vertexNormals.length; j++) {
						face.vertexNormals[j].z = 0;
						face.vertexNormals[j].normalize();
					}

					const va = textGeo.vertices[face.a];
					const vb = textGeo.vertices[face.b];
					const vc = textGeo.vertices[face.c];

					const s = triangle.set(va, vb, vc).getArea();

					if (s > triangleAreaHeuristics) {
						for (let j = 0; j < face.vertexNormals.length; j++) {
							face.vertexNormals[j].copy(face.normal);
						}
					}
				}
			}
		}

		textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);
		object.userData = {
			type: "#MESH",
			subtype: "#TEXT",
			size: size.getValue(),
			height: height.getValue(),
			bevelThickness: bevelThick.getValue(),
			bevelSize: bevel.getValue(),
			value: value.getValue(),
		};

		editor.execute(new SetGeometryCommand(editor, object, textGeo));
	}

	return container;
};

export { SidebarGeometryText };
