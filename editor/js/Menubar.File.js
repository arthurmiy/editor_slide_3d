/**
 * @author mrdoob / http://mrdoob.com/
 */

import * as THREE from "../../build/three.module.js";

import { ColladaExporter } from "../../examples/jsm/exporters/ColladaExporter.js";
import { DRACOExporter } from "../../examples/jsm/exporters/DRACOExporter.js";
import { GLTFExporter } from "../../examples/jsm/exporters/GLTFExporter.js";
import { OBJExporter } from "../../examples/jsm/exporters/OBJExporter.js";
import { PLYExporter } from "../../examples/jsm/exporters/PLYExporter.js";
import { STLExporter } from "../../examples/jsm/exporters/STLExporter.js";

import { JSZip } from "../../examples/jsm/libs/jszip.module.min.js";
import { Editor } from "./Editor.js";

import { UIPanel, UIRow, UIHorizontalRule } from "./libs/ui.js";

var MenubarFile = function (editor) {
	function parseNumber(key, value) {
		var precision = config.getKey("exportPrecision");

		return typeof value === "number"
			? parseFloat(value.toFixed(precision))
			: value;
	}

	//

	var config = editor.config;
	var strings = editor.strings;

	var container = new UIPanel();
	container.setClass("menu");

	var title = new UIPanel();
	title.setClass("title");
	title.setTextContent(strings.getKey("menubar/file"));
	container.add(title);

	var options = new UIPanel();
	options.setClass("options");
	container.add(options);

	// New

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/file/new"));
	option.onClick(function () {
		if (confirm("Any unsaved data will be lost. Are you sure?")) {
			editor.clear();
		}
	});
	options.add(option);

	//

	options.add(new UIHorizontalRule());

	// Import

	var form = document.createElement("form");
	form.style.display = "none";
	document.body.appendChild(form);

	var fileInput = document.createElement("input");
	fileInput.multiple = true;
	fileInput.type = "file";
	fileInput.addEventListener("change", function () {
		editor.loader.loadFiles(fileInput.files);
		form.reset();
	});
	form.appendChild(fileInput);

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/file/import"));
	option.onClick(function () {
		fileInput.click();
	});
	options.add(option);

	//

	// options.add(new UIHorizontalRule());

	// // Export Geometry

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/geometry' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected.' );
	// 		return;

	// 	}

	// 	var geometry = object.geometry;

	// 	if ( geometry === undefined ) {

	// 		alert( 'The selected object doesn\'t have geometry.' );
	// 		return;

	// 	}

	// 	var output = geometry.toJSON();

	// 	try {

	// 		output = JSON.stringify( output, parseNumber, '\t' );
	// 		output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	} catch ( e ) {

	// 		output = JSON.stringify( output );

	// 	}

	// 	saveString( output, 'geometry.json' );

	// } );
	// options.add( option );

	// // Export Object

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/object' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected' );
	// 		return;

	// 	}

	// 	var output = object.toJSON();

	// 	try {

	// 		output = JSON.stringify( output, parseNumber, '\t' );
	// 		output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	} catch ( e ) {

	// 		output = JSON.stringify( output );

	// 	}

	// 	saveString( output, 'model.json' );

	// } );
	// options.add( option );

	// // Export Scene

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/scene' ) );
	// option.onClick( function () {

	// 	var output = editor.scene.toJSON();

	// 	try {

	// 		output = JSON.stringify( output, parseNumber, '\t' );
	// 		output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	} catch ( e ) {

	// 		output = JSON.stringify( output );

	// 	}

	// 	saveString( output, 'scene.json' );

	// } );
	// options.add( option );

	// //

	// options.add( new UIHorizontalRule() );

	// // Export DAE

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/dae' ) );
	// option.onClick( function () {

	// 	var exporter = new ColladaExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveString( result.data, 'scene.dae' );

	// 	} );

	// } );
	// options.add( option );

	// // Save project

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/file/save_project"));
	option.onClick(function () {
		var output = editor.toJSON();
		output.metadata.type = "App";
		delete output.history;

		output = JSON.stringify(output, parseNumber, "\t");
		output = output.replace(/[\n\t]+([\d\.e\-\[\]]+)/g, "$1");

		var title = config.getKey("project/title");
		if (title == "") {
			title = "untitled";
		}

		saveString(output, title + ".json");
	});

	options.add(option);

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/drc' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null || object.isMesh === undefined ) {

	// 		alert( 'No mesh selected' );
	// 		return;

	// 	}

	// 	var exporter = new DRACOExporter();

	// 	// TODO: Change to DRACOExporter's parse( geometry, onParse )?
	// 	var result = exporter.parse( object.geometry );
	// 	saveArrayBuffer( result, 'model.drc' );

	// } );
	// options.add( option );

	// // Export GLB

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/glb' ) );
	// option.onClick( function () {

	// 	var exporter = new GLTFExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveArrayBuffer( result, 'scene.glb' );

	// 	}, { binary: true } );

	// } );
	// options.add( option );

	// // Export GLTF

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/gltf' ) );
	// option.onClick( function () {

	// 	var exporter = new GLTFExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveString( JSON.stringify( result, null, 2 ), 'scene.gltf' );

	// 	} );

	// } );
	// options.add( option );

	// // Export OBJ

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/obj' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected.' );
	// 		return;

	// 	}

	// 	var exporter = new OBJExporter();

	// 	saveString( exporter.parse( object ), 'model.obj' );

	// } );
	// options.add( option );

	// // Export PLY (ASCII)

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/ply' ) );
	// option.onClick( function () {

	// 	var exporter = new PLYExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveArrayBuffer( result, 'model.ply' );

	// 	} );

	// } );
	// options.add( option );

	// // Export PLY (Binary)

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/ply_binary' ) );
	// option.onClick( function () {

	// 	var exporter = new PLYExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveArrayBuffer( result, 'model-binary.ply' );

	// 	}, { binary: true } );

	// } );
	// options.add( option );

	// // Export STL (ASCII)

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/stl' ) );
	// option.onClick( function () {

	// 	var exporter = new STLExporter();

	// 	saveString( exporter.parse( editor.scene ), 'model.stl' );

	// } );
	// options.add( option );

	// // Export STL (Binary)

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/stl_binary' ) );
	// option.onClick( function () {

	// 	var exporter = new STLExporter();

	// 	saveArrayBuffer( exporter.parse( editor.scene, { binary: true } ), 'model-binary.stl' );

	// } );
	// options.add( option );

	// //

	// options.add( new UIHorizontalRule() );

	// Publish

	var loaded = 0;
	var toLoad = 0;

	function nodeToString(node) {
		var tmpNode = document.createElement("div");
		tmpNode.appendChild(node.cloneNode(true));
		var str = tmpNode.innerHTML;
		tmpNode = node = null; // prevent memory leaks in IE
		return str;
	}

	function correctAngle(angle) {
		if (angle < 0) {
			return correctAngle(angle + Math.PI * 2);
		} else if (angle > Math.PI * 2) {
			return correctAngle(angle - Math.PI * 2);
		} else {
			return angle;
		}
	}

	function minAngle(angle, previous) {
		if (previous != null) {
			if (previous - angle > Math.PI) {
				return minAngle(angle + 2 * Math.PI, previous);
			} else if (previous - angle < -Math.PI) {
				return minAngle(angle - 2 * Math.PI, previous);
			} else {
				return angle;
			}
		} else {
			return angle;
		}
	}

	var zip;

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/file/publish"));
	option.onClick(function () {
		zip = new JSZip();

		//

		// var output = editor.toJSON();
		// output.metadata.type = 'App';
		// delete output.history;

		// output = JSON.stringify( output, parseNumber, '\t' );
		// output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

		// zip.file( 'app.json', output );

		//

		var title = config.getKey("project/title");

		var manager = new THREE.LoadingManager(saveZip);

		var loader = new THREE.FileLoader(manager);

		/////////////////////////////////////customization
		loader.load("js/libs/app/customization/parameters.js", function (content) {
			var positionsList = "";
			var animationKeysList = "";
			var objs = "";
			var previousAngleX = null;
			var previousAngleY = null;
			var previousAngleZ = null;

			//construct positions list
			var slides = editor.slides;

			var animationPerSlide = [];

			if (slides !== undefined && slides.length > 0) {
				for (var i = 0; i < slides.length; i++) {
					let slide = slides[i];
					positionsList =
						positionsList +
						"{x:" +
						slide.x +
						",y:" +
						slide.y +
						",z:" +
						slide.z +
						",rx:" +
						minAngle(slide.rx, previousAngleX) +
						",ry:" +
						minAngle(slide.ry, previousAngleY) +
						",rz:" +
						minAngle(slide.rz, previousAngleZ) +
						",},";

					previousAngleX = minAngle(slide.rx, previousAngleX);
					previousAngleY = minAngle(slide.ry, previousAngleY);
					previousAngleZ = minAngle(slide.rz, previousAngleZ);

					//aproveita loop para preencher lista de inActions
					if (slide.inAction) {
						animationPerSlide.push(slide.inAction);
					} else {
						animationPerSlide.push("");
					}
				}
				content = content.replace("<!-- positions -->", positionsList);
			} else {
				content = content.replace("<!-- positions -->", "");
			}

			///////////////////////////construct animations keys list
			var aks = editor.animationKeys; //all animation keys
			var akString = ""; //final string to be included

			if (aks !== undefined && Object.keys(aks).length > 0) {
				for (var key in aks) {
					//look at each object in aks (stores object id in key)
					let aksObj = aks[key]; //stores all keys from a single obj
					//initialize object element with animation key (temporary because may not be added)
					let animationKeysGroup = "";

					for (var akID in aksObj) {
						let ak = aksObj[akID];
						//look each animationKey in a single obj
						let poseCounter = 0;

						let animationKeysTmp =
							ak.name + ': { objName:"' + key + '",poseKeys:[';

						if (ak.selected["rotation"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"rotation", type:"absolute", target:"obj", value:[' +
								ak.rotation[0] * THREE.MathUtils.DEG2RAD +
								"," +
								ak.rotation[1] * THREE.MathUtils.DEG2RAD +
								"," +
								ak.rotation[2] * THREE.MathUtils.DEG2RAD +
								"],},";
						}
						if (ak.selected["position"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"position", type:"absolute", target:"obj", value:[' +
								ak.position[0] +
								"," +
								ak.position[1] +
								"," +
								ak.position[2] +
								"],},";
						}
						if (ak.selected["scale"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"scale", type:"absolute", target:"obj", value:[' +
								ak.scale[0] +
								"," +
								ak.scale[1] +
								"," +
								ak.scale[2] +
								"],},";
						}
						if (ak.selected["opacity"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"Opacity", type:"absolute", target:"obj", value:' +
								ak.opacity +
								",},";
						}
						if (ak.selected["highlight"]) {
							poseCounter += 1;
							animationKeysTmp =
								animationKeysTmp +
								'{property:"EmissiveIntensity", type:"absolute", target:"obj", value:' +
								ak.highlight +
								",},";
						}

						if (poseCounter > 0) {
							//included some stuff
							animationKeysTmp = animationKeysTmp + "],},";
						} else {
							animationKeysTmp = "";
						}

						animationKeysGroup = animationKeysGroup + animationKeysTmp; //if doesnt have keys save add nothing
					}

					//adds object string if the object has valid keys
					if (animationKeysGroup != "") {
						//has valid keys
						akString =
							akString + '"' + key + '": {' + animationKeysGroup + "},";
					}
				}
				content = content.replace("<!-- ak -->", akString);
			} else {
				content = content.replace("<!-- ak -->", "");
			}

			////////////////////////////////////////////fill target and default keys
			var targetKeysString = "";
			var defaultKeysString = "";

			//todo set default on beginning

			for (var index in animationPerSlide) {
				if (animationPerSlide[index] != "") {
					var animationSplit = animationPerSlide[index].split("@"); //animation name in [0] keyName in [1]
					var objKeyAnimations = editor.animationKeys[animationSplit[0]];
					var defaultAnim;
					if (objKeyAnimations) {
						for (let keyA in objKeyAnimations) {
							if (objKeyAnimations[keyA].name == "default") {
								defaultAnim = "default";
							}
						}
						if (!defaultAnim) {
							defaultAnim = objKeyAnimations[0].name;
						}
					}

					targetKeysString =
						targetKeysString +
						"\nif (newSlide == " +
						index +
						') {\n targetKeys["' +
						animationSplit[0] +
						'"]="' +
						animationSplit[1] +
						'";}';
					defaultKeysString =
						defaultKeysString +
						"\nif (oldSlide == " +
						index +
						' && !targetKeys["' +
						animationSplit[0] +
						'"] ) {\n targetKeys["' +
						animationSplit[0] +
						'"]="' +
						defaultAnim +
						'";}';
				}
			}

			content = content.replace("//targetKeys//", targetKeysString);
			content = content.replace("//defaultKeys//", defaultKeysString);

			////////////////////////////////////////////add objects list
			(function addObjects(objects) {
				for (var i = 0, l = objects.length; i < l; i++) {
					var object = objects[i];

					if (object.userData.type == "#LIGHT") {
					} else {
						//add object to list

						if (object.userData.type == "#GROUP") {
							objs = objs + '{name: "' + object.uuid + '", children: [';
							addObjects(object.children);
							objs =
								objs +
								"], scale: [" +
								object.scale.x +
								"," +
								object.scale.y +
								"," +
								object.scale.z +
								"], rotation: [" +
								correctAngle(object.rotation.x) +
								"," +
								correctAngle(object.rotation.y) +
								"," +
								correctAngle(object.rotation.z) +
								"],position: [{ x:" +
								object.position.x +
								",y:" +
								object.position.y +
								",z:" +
								object.position.z +
								"}],},";
						} else if (object.userData.type == "#SPRITE") {
							let prefix = object.uuid;

							objs =
								objs +
								'{name:"' +
								prefix +
								'", path: "./static/models/' +
								prefix +
								'.json", scale: [' +
								object.scale.x +
								"," +
								object.scale.y +
								"," +
								object.scale.z +
								"], rotation: [" +
								correctAngle(object.rotation.x) +
								"," +
								correctAngle(object.rotation.y) +
								"," +
								correctAngle(object.rotation.z) +
								"],position: [{ x:" +
								object.position.x +
								",y:" +
								object.position.y +
								",z:" +
								object.position.z +
								"}],},";

							//export and add
							var output = object.toJSON();

							try {
								output = JSON.stringify(output, parseNumber, "\t");
								output = output.replace(/[\n\t]+([\d\.e\-\[\]]+)/g, "$1");
							} catch (e) {
								output = JSON.stringify(output);
							}

							zip.file("static/models/" + prefix + ".json", output);
						} else if (object.userData.type == "#MESH") {
							let prefix = object.uuid;

							objs =
								objs +
								'{name:"' +
								object.uuid +
								'", path: "./static/models/' +
								prefix +
								'.json", scale: [' +
								object.scale.x +
								"," +
								object.scale.y +
								"," +
								object.scale.z +
								"], rotation: [" +
								correctAngle(object.rotation.x) +
								"," +
								correctAngle(object.rotation.y) +
								"," +
								correctAngle(object.rotation.z) +
								"],position: [{ x:" +
								object.position.x +
								",y:" +
								object.position.y +
								",z:" +
								object.position.z +
								"}],},";

							//export and add
							// var exporter = new GLTFExporter();

							// let tmp = object.clone();
							// tmp.rotation.copy(new THREE.Euler(0, 0, 0));
							// tmp.scale.copy(new THREE.Vector3(1, 1, 1));
							// tmp.position.copy(new THREE.Vector3(0, 0, 0));

							// tmp.updateMatrixWorld(true);

							// toLoad += 1;
							// exporter.parse(
							// 	tmp,
							// 	function (result) {
							// 		zip.file("static/models/" + prefix + ".glb", result);
							// 		console.log("added" + prefix);
							// 		loaded += 1;
							// 	},
							// 	{ binary: true }
							// );

							//export and add
							var output = object.toJSON();
							output.object.name = object.uuid;

							try {
								output = JSON.stringify(output, parseNumber, "\t");
								output = output.replace(/[\n\t]+([\d\.e\-\[\]]+)/g, "$1");
							} catch (e) {
								output = JSON.stringify(output);
							}
							zip.file("static/models/" + prefix + ".json", output);
						} else {
							let prefix = object.name;
							if (prefix.indexOf(".") != -1) {
								prefix = prefix.substring(0, prefix.indexOf("."));
							}
							objs =
								objs +
								'{name:"' +
								object.uuid +
								'", path: "./static/models/' +
								object.name +
								'", scale: [' +
								object.scale.x +
								"," +
								object.scale.y +
								"," +
								object.scale.z +
								"], rotation: [" +
								correctAngle(object.rotation.x) +
								"," +
								correctAngle(object.rotation.y) +
								"," +
								correctAngle(object.rotation.z) +
								"],position: [{ x:" +
								object.position.x +
								",y:" +
								object.position.y +
								",z:" +
								object.position.z +
								"}],},";
						}
					}
				}
			})(editor.scene.children);

			content = content.replace("<!-- objs -->", objs);

			content = content.replace("<!-- code -->", editor.scripts);

			zip.file("static/customization/parameters.js", content);
		});

		function saveZip() {
			var title = config.getKey("project/title");
			if (loaded < toLoad) {
				setTimeout(saveZip, 1000);
			} else {
				save(
					zip.generate({ type: "blob" }),
					(title !== "" ? title : "untitled") + ".zip"
				);
				zip = null;
			}
		}

		///////////////index

		loader.load("js/libs/app/index.html", function (content) {
			content = content.replace("<!-- title -->", title);

			var slides = editor.slides;

			var slidesHtml = "";

			if (slides !== undefined && slides.length > 0) {
				for (var i = 0; i < slides.length; i++) {
					let parentDiv = document.createElement("div");
					let div = document.createElement("div");
					if (slides[i].type == "title") {
						parentDiv.classList.add("fullscrn");
						div.classList.add("title");
					} else if (slides[i].type == "left") {
						parentDiv.classList.add("area");

						div.classList.add("leftAll");
					} else if (slides[i].type == "right") {
						parentDiv.classList.add("area");

						div.classList.add("rightAll");
					} else if (slides[i].type == "none") {
						parentDiv.classList.add("fullscrn");
					}
					div.innerHTML = slides[i].source;

					parentDiv.appendChild(div);
					if (slides[i].type == "left" || slides[i].type == "right") {
						let tmpDiv = document.createElement("div");
						tmpDiv.classList.add("fullscrn");
						tmpDiv.appendChild(parentDiv);
						slidesHtml = slidesHtml + nodeToString(tmpDiv);
					} else {
						slidesHtml = slidesHtml + nodeToString(parentDiv);
					}
				}
			}

			content = content.replace("\n\t\t\t/* content */\n", slidesHtml);

			zip.file("index.html", content);
		});

		//css
		loader.load("js/libs/app/css/parent.css", function (content) {
			zip.file("static/css/parent.css", content);
		});

		///////////////////////////////////////fonts
		loader.load(
			"js/libs/app/fonts/gentilis_bold.typeface.json",
			function (content) {
				zip.file("static/fonts/gentilis_bold.typeface.json", content);
			}
		);
		loader.load(
			"js/libs/app/fonts/gentilis_regular.typeface.json",
			function (content) {
				zip.file("static/fonts/gentilis_regular.typeface.json", content);
			}
		);

		loader.load(
			"js/libs/app/fonts/helvetiker_bold.typeface.json",
			function (content) {
				zip.file("static/fonts/helvetiker_bold.typeface.json", content);
			}
		);
		loader.load(
			"js/libs/app/fonts/helvetiker_regular.typeface.json",
			function (content) {
				zip.file("static/fonts/helvetiker_regular.typeface.json", content);
			}
		);

		loader.load(
			"js/libs/app/fonts/optimer_bold.typeface.json",
			function (content) {
				zip.file("static/fonts/optimer_bold.typeface.json", content);
			}
		);
		loader.load(
			"js/libs/app/fonts/optimer_regular.typeface.json",
			function (content) {
				zip.file("static/fonts/optimer_regular.typeface.json", content);
			}
		);

		//////////////////////////////////imgs
		loader.load("js/libs/app/imgs/readme.txt", function (content) {
			zip.file("static/imgs/readme.txt", content);
		});

		/////////////////////////////java
		//	controls
		loader.load(
			"js/libs/app/java/controls/OrbitControls.js",
			function (content) {
				zip.file("static/java/controls/OrbitControls.js", content);
			}
		);

		//	curves
		loader.load("js/libs/app/java/curves/NURBSCurve.js", function (content) {
			zip.file("static/java/curves/NURBSCurve.js", content);
		});
		loader.load("js/libs/app/java/curves/NURBSUtils.js", function (content) {
			zip.file("static/java/curves/NURBSUtils.js", content);
		});

		//	libs
		loader.load(
			"js/libs/app/java/libs/inflate.module.min.js",
			function (content) {
				zip.file("static/java/libs/inflate.module.min.js", content);
			}
		);
		loader.load(
			"js/libs/app/java/libs/tween.module.min.js",
			function (content) {
				zip.file("static/java/libs/tween.module.min.js", content);
			}
		);

		///////loaders
		//gltf
		loader.load(
			"js/libs/app/java/loaders/gltf/draco_decoder.js",
			function (content) {
				zip.file("static/java/loaders/gltf/draco_decoder.js", content);
			}
		);

		loader.load(
			"js/libs/app/java/loaders/gltf/draco_encoder.js",
			function (content) {
				zip.file("static/java/loaders/gltf/draco_encoder.js", content);
			}
		);
		loader.load(
			"js/libs/app/java/loaders/gltf/draco_wasm_wrapper.js",
			function (content) {
				zip.file("static/java/loaders/gltf/draco_wasm_wrapper.js", content);
			}
		);

		loader.load(
			"js/libs/app/java/loaders/draco_decoder.js",
			function (content) {
				zip.file("static/java/loaders/draco_decoder.js", content);
			}
		);
		loader.load(
			"js/libs/app/java/loaders/draco_decoder.wasm",
			function (content) {
				zip.file("static/java/loaders/draco_decoder.wasm", content);
			}
		);

		loader.load(
			"js/libs/app/java/loaders/draco_encoder.js",
			function (content) {
				zip.file("static/java/loaders/draco_encoder.js", content);
			}
		);
		loader.load(
			"js/libs/app/java/loaders/draco_wasm_wrapper.js",
			function (content) {
				zip.file("static/java/loaders/draco_wasm_wrapper.js", content);
			}
		);

		loader.load(
			"js/libs/app/java/loaders/DRACOLoader.d.ts",
			function (content) {
				zip.file("static/java/loaders/DRACOLoader.d.ts", content);
			}
		);
		loader.load("js/libs/app/java/loaders/DRACOLoader.js", function (content) {
			zip.file("static/java/loaders/DRACOLoader.js", content);
		});

		loader.load("js/libs/app/java/loaders/FBXLoader.d.ts", function (content) {
			zip.file("static/java/loaders/FBXLoader.d.ts", content);
		});
		loader.load("js/libs/app/java/loaders/FBXLoader.js", function (content) {
			zip.file("static/java/loaders/FBXLoader.js", content);
		});

		loader.load("js/libs/app/java/loaders/GLTFLoader.d.ts", function (content) {
			zip.file("static/java/loaders/GLTFLoader.d.ts", content);
		});
		loader.load("js/libs/app/java/loaders/GLTFLoader.js", function (content) {
			zip.file("static/java/loaders/GLTFLoader.js", content);
		});

		/////////////////////models
		loader.load("js/libs/app/models/readme.txt", function (content) {
			zip.file("static/models/readme.txt", content);
		});

		//root
		loader.load("js/libs/app/java/controllerModule.js", function (content) {
			zip.file("static/java/controllerModule.js", content);
		});
		loader.load("js/libs/app/java/corto.em.js", function (content) {
			zip.file("static/java/corto.em.js", content);
		});

		loader.load("js/libs/app/java/nexus_three.js", function (content) {
			zip.file("static/java/nexus_three.js", content);
		});
		loader.load("js/libs/app/java/pagecontrollerHtml.js", function (content) {
			zip.file("static/java/pagecontrollerHtml.js", content);
		});
		loader.load("js/libs/app/java/three.js", function (content) {
			zip.file("static/java/three.js", content);
		});

		loader.load("js/libs/app/java/three.min.js", function (content) {
			zip.file("static/java/three.min.js", content);
		});
		loader.load("js/libs/app/java/three.module.js", function (content) {
			zip.file("static/java/three.module.js", content);
		});

		loader.load("js/libs/app/java/tween.js", function (content) {
			zip.file("static/java/tween.js", content);
		});

		//////////////////////////binary
		loader.responseType = "arraybuffer";
		loader.load(
			"js/libs/app/java/loaders/gltf/draco_decoder.wasm",
			function (content) {
				zip.file("static/java/loaders/gltf/draco_decoder.wasm", content);
			}
		);

		loader.load("js/libs/app/start.exe", function (content) {
			zip.file("start.exe", content);
		});
	});
	options.add(option);

	//

	var link = document.createElement("a");
	function save(blob, filename) {
		link.href = URL.createObjectURL(blob);
		link.download = filename || "data.json";
		link.dispatchEvent(new MouseEvent("click"));

		// URL.revokeObjectURL( url ); breaks Firefox...
	}

	function saveArrayBuffer(buffer, filename) {
		save(new Blob([buffer], { type: "application/octet-stream" }), filename);
	}

	function saveString(text, filename) {
		save(new Blob([text], { type: "text/plain" }), filename);
	}

	return container;
};

export { MenubarFile };
