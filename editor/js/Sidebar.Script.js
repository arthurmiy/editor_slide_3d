/**
 * @author mrdoob / http://mrdoob.com/
 */

import {
	UIPanel,
	UIBreak,
	UIText,
	UIButton,
	UIRow,
	UIInput,
} from "./libs/ui.js";

import { AddScriptCommand } from "./commands/AddScriptCommand.js";
import { SetScriptValueCommand } from "./commands/SetScriptValueCommand.js";
import { RemoveScriptCommand } from "./commands/RemoveScriptCommand.js";

var SidebarScript = function (editor) {
	var strings = editor.strings;

	var signals = editor.signals;

	var container = new UIPanel();
	// container.setDisplay("none");

	container.add(new UIText("Script")).setTextTransform("uppercase");
	container.add(new UIBreak());
	container.add(new UIBreak());

	//

	var scriptsContainer = new UIRow();
	container.add(scriptsContainer);

	var edit = new UIButton(strings.getKey("sidebar/script/edit"));
	edit.setMarginLeft("4px");
	edit.onClick(function () {
		signals.editScript.dispatch(null, {
			name: "Custom Script",
			source: editor.scripts,
		});
	});
	scriptsContainer.add(edit);

	var remove = new UIButton("RESET");
	remove.setMarginLeft("4px");
	remove.onClick(function () {
		if (confirm("Are you sure?")) {
			editor.execute(new RemoveScriptCommand(editor, editor.selected, script));
		}
	});
	scriptsContainer.add(remove);

	scriptsContainer.add(new UIBreak());

	// var newScript = new UIButton(strings.getKey("sidebar/script/new"));
	// newScript.onClick(function () {
	// 	var script = { name: "", source: "function update( event ) {}" };
	// 	editor.execute(new AddScriptCommand(editor, editor.selected, script));
	// });
	// container.add(newScript);

	/*
	var loadScript = new UI.Button( 'Load' );
	loadScript.setMarginLeft( '4px' );
	container.add( loadScript );
	*/

	//

	return container;
};

export { SidebarScript };
