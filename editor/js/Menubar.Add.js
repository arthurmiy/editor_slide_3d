/**
 * @author mrdoob / http://mrdoob.com/
 */

import * as THREE from "../../build/three.module.js";

import { UIPanel, UIRow, UIHorizontalRule } from "./libs/ui.js";

import { AddObjectCommand } from "./commands/AddObjectCommand.js";

var MenubarAdd = function (editor) {
	var strings = editor.strings;

	var container = new UIPanel();
	container.setClass("menu");

	var title = new UIPanel();
	title.setClass("title");
	title.setTextContent(strings.getKey("menubar/add"));
	container.add(title);

	var options = new UIPanel();
	options.setClass("options");
	container.add(options);

	// Group

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/group"));
	option.onClick(function () {
		var mesh = new THREE.Group();
		mesh.name = "Group";
		mesh.userData = { type: "#GROUP" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	//

	options.add(new UIHorizontalRule());

	// Box

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/box"));
	option.onClick(function () {
		var geometry = new THREE.BoxBufferGeometry(1, 1, 1, 1, 1, 1);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Box";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Circle

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/circle"));
	option.onClick(function () {
		var geometry = new THREE.CircleBufferGeometry(1, 8, 0, Math.PI * 2);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Circle";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Cylinder

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/cylinder"));
	option.onClick(function () {
		var geometry = new THREE.CylinderBufferGeometry(
			1,
			1,
			1,
			8,
			1,
			false,
			0,
			Math.PI * 2
		);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Cylinder";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Dodecahedron

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/dodecahedron"));
	option.onClick(function () {
		var geometry = new THREE.DodecahedronBufferGeometry(1, 0);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Dodecahedron";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Icosahedron

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/icosahedron"));
	option.onClick(function () {
		var geometry = new THREE.IcosahedronBufferGeometry(1, 0);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Icosahedron";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Lathe

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/lathe"));
	option.onClick(function () {
		var points = [
			new THREE.Vector2(0, 0),
			new THREE.Vector2(0.4, 0),
			new THREE.Vector2(0.35, 0.05),
			new THREE.Vector2(0.1, 0.075),
			new THREE.Vector2(0.08, 0.1),
			new THREE.Vector2(0.08, 0.4),
			new THREE.Vector2(0.1, 0.42),
			new THREE.Vector2(0.14, 0.48),
			new THREE.Vector2(0.2, 0.5),
			new THREE.Vector2(0.25, 0.54),
			new THREE.Vector2(0.3, 1.2),
		];

		var geometry = new THREE.LatheBufferGeometry(points, 12, 0, Math.PI * 2);
		var mesh = new THREE.Mesh(
			geometry,
			new THREE.MeshStandardMaterial({ side: THREE.DoubleSide })
		);
		mesh.name = "Lathe";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Octahedron

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/octahedron"));
	option.onClick(function () {
		var geometry = new THREE.OctahedronBufferGeometry(1, 0);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Octahedron";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Plane

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/plane"));
	option.onClick(function () {
		var geometry = new THREE.PlaneBufferGeometry(1, 1, 1, 1);
		var material = new THREE.MeshStandardMaterial();
		var mesh = new THREE.Mesh(geometry, material);
		mesh.name = "Plane";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Ring

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/ring"));
	option.onClick(function () {
		var geometry = new THREE.RingBufferGeometry(0.5, 1, 8, 1, 0, Math.PI * 2);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Ring";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Sphere

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/sphere"));
	option.onClick(function () {
		var geometry = new THREE.SphereBufferGeometry(
			1,
			8,
			6,
			0,
			Math.PI * 2,
			0,
			Math.PI
		);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Sphere";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Sprite

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/sprite"));
	option.onClick(function () {
		var sprite = new THREE.Sprite(new THREE.SpriteMaterial());
		sprite.name = "Sprite";
		sprite.userData = { type: "#SPRITE" };

		editor.execute(new AddObjectCommand(editor, sprite));
	});
	options.add(option);

	// Tetrahedron

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/tetrahedron"));
	option.onClick(function () {
		var geometry = new THREE.TetrahedronBufferGeometry(1, 0);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Tetrahedron";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Torus

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/torus"));
	option.onClick(function () {
		var geometry = new THREE.TorusBufferGeometry(1, 0.4, 8, 6, Math.PI * 2);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Torus";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// TorusKnot

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/torusknot"));
	option.onClick(function () {
		var geometry = new THREE.TorusKnotBufferGeometry(1, 0.4, 64, 8, 2, 3);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "TorusKnot";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Tube

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/tube"));
	option.onClick(function () {
		var path = new THREE.CatmullRomCurve3([
			new THREE.Vector3(2, 2, -2),
			new THREE.Vector3(2, -2, -0.6666666666666667),
			new THREE.Vector3(-2, -2, 0.6666666666666667),
			new THREE.Vector3(-2, 2, 2),
		]);

		var geometry = new THREE.TubeBufferGeometry(path, 64, 1, 8, false);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = "Tube";
		mesh.userData = { type: "#MESH" };

		editor.execute(new AddObjectCommand(editor, mesh));
	});
	options.add(option);

	// Text

	var option = new UIRow();
	option.setClass("option");
	option.setTextContent(strings.getKey("menubar/add/text"));
	option.onClick(function () {
		///////////////////////////////////////////////////////////////////////////////////////////////
		let extracted = "texto";
		let font = editor.font;
		let textGeo = new THREE.TextGeometry(extracted, {
			font,

			size: 10,
			height: 1,
			curveSegments: 4,

			bevelThickness: 1,
			bevelSize: 0.1,
			bevelEnabled: true,
		});

		textGeo.computeBoundingBox();
		textGeo.computeVertexNormals();

		const triangle = new THREE.Triangle();

		// "fix" side normals by removing z-component of normals for side faces
		// (this doesn't work well for beveled geometry as then we lose nice curvature around z-axis)

		let bevelEnabled = true;

		if (!bevelEnabled) {
			const triangleAreaHeuristics = 0.1 * (height * size);

			for (let i = 0; i < textGeo.faces.length; i++) {
				const face = textGeo.faces[i];

				if (face.materialIndex == 1) {
					for (let j = 0; j < face.vertexNormals.length; j++) {
						face.vertexNormals[j].z = 0;
						face.vertexNormals[j].normalize();
					}

					const va = textGeo.vertices[face.a];
					const vb = textGeo.vertices[face.b];
					const vc = textGeo.vertices[face.c];

					const s = triangle.set(va, vb, vc).getArea();

					if (s > triangleAreaHeuristics) {
						for (let j = 0; j < face.vertexNormals.length; j++) {
							face.vertexNormals[j].copy(face.normal);
						}
					}
				}
			}
		}

		textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);

		let textMesh1 = new THREE.Mesh(textGeo, new THREE.MeshStandardMaterial());

		textMesh1.name = "Text";
		textMesh1.userData = {
			type: "#MESH",
			subtype: "#TEXT",
			value: "texto",
			size: 10,
			height: 1,
			bevelThickness: 1,
			bevelSize: 0.1,
		};

		///////////////////////////////////////////////////////////////////////////////////////////////

		editor.execute(new AddObjectCommand(editor, textMesh1));
	});
	options.add(option);

	/*
	// Teapot

	var option = new UIRow();
	option.setClass( 'option' );
	option.setTextContent( 'Teapot' );
	option.onClick( function () {

		var size = 50;
		var segments = 10;
		var bottom = true;
		var lid = true;
		var body = true;
		var fitLid = false;
		var blinnScale = true;

		var material = new THREE.MeshStandardMaterial();

		var geometry = new TeapotBufferGeometry( size, segments, bottom, lid, body, fitLid, blinnScale );
		var mesh = new THREE.Mesh( geometry, material );
		mesh.name = 'Teapot';

		editor.addObject( mesh );
		editor.select( mesh );

	} );
	options.add( option );
	*/

	//

	// options.add(new UIHorizontalRule());

	// AmbientLight

	// var option = new UIRow();
	// option.setClass("option");
	// option.setTextContent(strings.getKey("menubar/add/ambientlight"));
	// option.onClick(function () {
	// 	var color = 0x222222;

	// 	var light = new THREE.AmbientLight(color);
	// 	light.name = "AmbientLight";

	// 	editor.execute(new AddObjectCommand(editor, light));
	// });
	// options.add(option);

	// // DirectionalLight

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/directionallight' ) );
	// option.onClick( function () {

	// 	var color = 0xffffff;
	// 	var intensity = 1;

	// 	var light = new THREE.DirectionalLight( color, intensity );
	// 	light.name = 'DirectionalLight';
	// 	light.target.name = 'DirectionalLight Target';

	// 	light.position.set( 5, 10, 7.5 );

	// 	editor.execute( new AddObjectCommand( editor, light ) );

	// } );
	// options.add( option );

	// // HemisphereLight

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/hemispherelight' ) );
	// option.onClick( function () {

	// 	var skyColor = 0x00aaff;
	// 	var groundColor = 0xffaa00;
	// 	var intensity = 1;

	// 	var light = new THREE.HemisphereLight( skyColor, groundColor, intensity );
	// 	light.name = 'HemisphereLight';

	// 	light.position.set( 0, 10, 0 );

	// 	editor.execute( new AddObjectCommand( editor, light ) );

	// } );
	// options.add( option );

	// // PointLight

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/pointlight' ) );
	// option.onClick( function () {

	// 	var color = 0xffffff;
	// 	var intensity = 1;
	// 	var distance = 0;

	// 	var light = new THREE.PointLight( color, intensity, distance );
	// 	light.name = 'PointLight';

	// 	editor.execute( new AddObjectCommand( editor, light ) );

	// } );
	// options.add( option );

	// // SpotLight

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/spotlight' ) );
	// option.onClick( function () {

	// 	var color = 0xffffff;
	// 	var intensity = 1;
	// 	var distance = 0;
	// 	var angle = Math.PI * 0.1;
	// 	var penumbra = 0;

	// 	var light = new THREE.SpotLight( color, intensity, distance, angle, penumbra );
	// 	light.name = 'SpotLight';
	// 	light.target.name = 'SpotLight Target';

	// 	light.position.set( 5, 10, 7.5 );

	// 	editor.execute( new AddObjectCommand( editor, light ) );

	// } );
	// options.add( option );

	// //

	// options.add( new UIHorizontalRule() );

	// // OrthographicCamera

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/orthographiccamera' ) );
	// option.onClick( function () {

	// 	var camera = new THREE.OrthographicCamera();
	// 	camera.name = 'OrthographicCamera';

	// 	editor.execute( new AddObjectCommand( editor, camera ) );

	// } );
	// options.add( option );

	// // PerspectiveCamera

	// var option = new UIRow();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/perspectivecamera' ) );
	// option.onClick( function () {

	// 	var camera = new THREE.PerspectiveCamera();
	// 	camera.name = 'PerspectiveCamera';

	// 	editor.execute( new AddObjectCommand( editor, camera ) );

	// } );
	// options.add( option );

	return container;
};

export { MenubarAdd };
