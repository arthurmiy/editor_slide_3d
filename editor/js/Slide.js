/**
 * @author mrdoob / http://mrdoob.com/
 */

import {
	UIElement,
	UIPanel,
	UIText,
	UIRow,
	UISelect,
	UITextArea,
	UIDiv,
} from "./libs/ui.js";

import { SetSlideValueCommand } from "./commands/SetSlideValueCommand.js";
import { SetMaterialValueCommand } from "./commands/SetMaterialValueCommand.js";

var Slide = function (editor) {
	var signals = editor.signals;

	var strings = editor.strings;

	var container = new UIPanel();
	container.setId("slide");
	container.setPosition("absolute");
	container.setBackgroundColor("#99272822");
	container.setDisplay("none");

	var header = new UIPanel();
	header.setBackgroundColor("#272822");
	header.setPadding("10px");
	container.add(header);

	var title = new UIText().setColor("#fff");
	header.add(title);

	var buttonSVG = (function () {
		var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		svg.setAttribute("width", 32);
		svg.setAttribute("height", 32);
		var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
		path.setAttribute("d", "M 12,12 L 22,22 M 22,12 12,22");
		path.setAttribute("stroke", "#fff");
		svg.appendChild(path);
		return svg;
	})();

	var close = new UIElement(buttonSVG);
	close.setPosition("absolute");
	close.setTop("3px");
	close.setRight("1px");
	close.setCursor("pointer");
	close.onClick(function () {
		container.setDisplay("none");
	});
	header.add(close);

	var renderer;

	signals.rendererChanged.add(function (newRenderer) {
		renderer = newRenderer;
	});

	var currentSlide;

	///////////////////////
	var uidiv = new UIDiv();
	uidiv.setBackgroundColor("#ffffff");

	///////Seletor de layout

	var lrow = new UIRow();
	lrow.setPadding("10px");
	lrow.add(
		new UIText(strings.getKey("slide/layout_type"))
			.setWidth("90px")
			.setColor("#fff")
	);
	lrow.setPadding("10px");
	var layoutSelect = new UISelect();

	layoutSelect.onChange(function () {
		//todo fazer mudanças de acordo com layout
		// editor.setViewportCamera( this.getValue() );
	});

	var options = {};
	options["none"] = "";
	options["title"] = strings.getKey("slide/title");
	options["left"] = strings.getKey("slide/card_left");
	options["right"] = strings.getKey("slide/card_right");
	// options[ 'center' ] = 'Cartão no centro';

	layoutSelect.setOptions(options);
	layoutSelect.setValue("none");
	layoutSelect.onChange(function () {
		editor.execute(
			new SetSlideValueCommand(editor, currentSlide, "type", this.getValue())
		);
	});

	lrow.add(layoutSelect);

	container.add(lrow);

	///////In action

	var inActionRow = new UIRow();
	inActionRow.setPadding("10px");
	inActionRow.add(
		new UIText(strings.getKey("slide/in-action"))
			.setWidth("90px")
			.setColor("#fff")
	);
	inActionRow.setPadding("10px");
	var inActionSelect = new UISelect();

	inActionSelect.onChange(function () {
		//todo fazer mudanças de acordo com layout
		// editor.setViewportCamera( this.getValue() );
	});

	var inActionOptions = {};
	// options[ 'center' ] = 'Cartão no centro';

	inActionSelect.setOptions(inActionOptions);
	// layoutSelect.setValue("none");
	inActionSelect.onChange(function () {
		editor.execute(
			new SetSlideValueCommand(
				editor,
				currentSlide,
				"inAction",
				this.getValue()
			)
		);
	});

	inActionRow.add(inActionSelect);

	container.add(inActionRow);
	inActionRow.setDisplay("none");

	///////////////////// body
	var body = new UIPanel();
	body.setPadding("10px");
	container.add(body);

	var objectTypeRow = new UIRow();

	//Title
	objectTypeRow.add(
		new UIText(strings.getKey("slide/html_content"))
			.setWidth("200px")
			.setColor("#fff")
			.setFontSize("20px")
	);
	objectTypeRow.setPadding("10px");
	body.add(objectTypeRow);

	//text area
	var objectUserDataRow = new UIRow();
	var objectUserData = new UITextArea()
		.setWidth("400px")
		.setHeight("100px")
		.setFontSize("12px");

	objectUserData.onKeyUp(function () {
		updateUI(objectUserData);
		editor.execute(
			new SetSlideValueCommand(editor, currentSlide, "source", this.getValue())
		);
	});
	// objectUserDataRow.add( new UIText( 'HTML' ).setWidth( '90px' ) );
	objectUserDataRow.add(objectUserData);

	body.add(objectUserDataRow);

	//previa
	var previa = new UIRow();
	previa.add(
		new UIText(strings.getKey("slide/Preview"))
			.setWidth("200px")
			.setColor("#fff")
			.setFontSize("20px")
	);
	previa.setPadding("10px");
	body.add(previa);

	body.add(uidiv);

	// var codemirror = CodeMirror( container.dom, {
	// 	value: '',
	// 	lineNumbers: true,
	// 	matchBrackets: true,
	// 	indentWithTabs: true,
	// 	tabSize: 4,
	// 	indentUnit: 4,
	// 	hintOptions: {
	// 		completeSingle: false
	// 	}
	// } );
	// codemirror.setOption( 'theme', 'monokai' );
	// codemirror.on( 'change', function () {

	// 	if ( codemirror.state.focused === false ) return;

	// 	clearTimeout( delay );
	// 	delay = setTimeout( function () {

	// 		var value = codemirror.getValue();

	// 		if ( ! validate( value ) ) return;

	// 		if ( typeof ( currentScript ) === 'object' ) {

	// 			if ( value !== currentScript.source ) {

	// 				editor.execute( new SetScriptValueCommand( editor, currentObject, currentScript, 'source', value ) );

	// 			}
	// 			return;

	// 		}

	// 		if ( currentScript !== 'programInfo' ) return;

	// 		var json = JSON.parse( value );

	// 		if ( JSON.stringify( currentObject.material.defines ) !== JSON.stringify( json.defines ) ) {

	// 			var cmd = new SetMaterialValueCommand( editor, currentObject, 'defines', json.defines );
	// 			cmd.updatable = false;
	// 			editor.execute( cmd );

	// 		}
	// 		if ( JSON.stringify( currentObject.material.uniforms ) !== JSON.stringify( json.uniforms ) ) {

	// 			var cmd = new SetMaterialValueCommand( editor, currentObject, 'uniforms', json.uniforms );
	// 			cmd.updatable = false;
	// 			editor.execute( cmd );

	// 		}
	// 		if ( JSON.stringify( currentObject.material.attributes ) !== JSON.stringify( json.attributes ) ) {

	// 			var cmd = new SetMaterialValueCommand( editor, currentObject, 'attributes', json.attributes );
	// 			cmd.updatable = false;
	// 			editor.execute( cmd );

	// 		}

	// 	}, 300 );

	// } );

	// // prevent backspace from deleting objects
	// var wrapper = codemirror.getWrapperElement();
	// wrapper.addEventListener( 'keydown', function ( event ) {

	// 	event.stopPropagation();

	// } );

	// // validate

	// var errorLines = [];
	// var widgets = [];

	// var validate = function ( string ) {

	// 	var valid;
	// 	var errors = [];

	// 	return codemirror.operation( function () {

	// 		while ( errorLines.length > 0 ) {

	// 			codemirror.removeLineClass( errorLines.shift(), 'background', 'errorLine' );

	// 		}

	// 		while ( widgets.length > 0 ) {

	// 			codemirror.removeLineWidget( widgets.shift() );

	// 		}

	// 		//

	// 		switch ( currentMode ) {

	// 			case 'javascript':

	// 				try {

	// 					var syntax = esprima.parse( string, { tolerant: true } );
	// 					errors = syntax.errors;

	// 				} catch ( error ) {

	// 					errors.push( {

	// 						lineNumber: error.lineNumber - 1,
	// 						message: error.message

	// 					} );

	// 				}

	// 				for ( var i = 0; i < errors.length; i ++ ) {

	// 					var error = errors[ i ];
	// 					error.message = error.message.replace( /Line [0-9]+: /, '' );

	// 				}

	// 				break;

	// 			case 'json':

	// 				errors = [];

	// 				jsonlint.parseError = function ( message, info ) {

	// 					message = message.split( '\n' )[ 3 ];

	// 					errors.push( {

	// 						lineNumber: info.loc.first_line - 1,
	// 						message: message

	// 					} );

	// 				};

	// 				try {

	// 					jsonlint.parse( string );

	// 				} catch ( error ) {

	// 					// ignore failed error recovery

	// 				}

	// 				break;

	// 			case 'glsl':

	// 				try {

	// 					var shaderType = currentScript === 'vertexShader' ?
	// 						glslprep.Shader.VERTEX : glslprep.Shader.FRAGMENT;

	// 					glslprep.parseGlsl( string, shaderType );

	// 				} catch ( error ) {

	// 					if ( error instanceof glslprep.SyntaxError ) {

	// 						errors.push( {

	// 							lineNumber: error.line,
	// 							message: "Syntax Error: " + error.message

	// 						} );

	// 					} else {

	// 						console.error( error.stack || error );

	// 					}

	// 				}

	// 				if ( errors.length !== 0 ) break;
	// 				if ( renderer instanceof THREE.WebGLRenderer === false ) break;

	// 				currentObject.material[ currentScript ] = string;
	// 				currentObject.material.needsUpdate = true;
	// 				signals.materialChanged.dispatch( currentObject.material );

	// 				var programs = renderer.info.programs;

	// 				valid = true;
	// 				var parseMessage = /^(?:ERROR|WARNING): \d+:(\d+): (.*)/g;

	// 				for ( var i = 0, n = programs.length; i !== n; ++ i ) {

	// 					var diagnostics = programs[ i ].diagnostics;

	// 					if ( diagnostics === undefined ||
	// 							diagnostics.material !== currentObject.material ) continue;

	// 					if ( ! diagnostics.runnable ) valid = false;

	// 					var shaderInfo = diagnostics[ currentScript ];
	// 					var lineOffset = shaderInfo.prefix.split( /\r\n|\r|\n/ ).length;

	// 					while ( true ) {

	// 						var parseResult = parseMessage.exec( shaderInfo.log );
	// 						if ( parseResult === null ) break;

	// 						errors.push( {

	// 							lineNumber: parseResult[ 1 ] - lineOffset,
	// 							message: parseResult[ 2 ]

	// 						} );

	// 					} // messages

	// 					break;

	// 				} // programs

	// 		} // mode switch

	// 		for ( var i = 0; i < errors.length; i ++ ) {

	// 			var error = errors[ i ];

	// 			var message = document.createElement( 'div' );
	// 			message.className = 'esprima-error';
	// 			message.textContent = error.message;

	// 			var lineNumber = Math.max( error.lineNumber, 0 );
	// 			errorLines.push( lineNumber );

	// 			codemirror.addLineClass( lineNumber, 'background', 'errorLine' );

	// 			var widget = codemirror.addLineWidget( lineNumber, message );

	// 			widgets.push( widget );

	// 		}

	// 		return valid !== undefined ? valid : errors.length === 0;

	// 	} );

	// };

	// // tern js autocomplete

	// var server = new CodeMirror.TernServer( {
	// 	caseInsensitive: true,
	// 	plugins: { threejs: null }
	// } );

	// codemirror.setOption( 'extraKeys', {
	// 	'Ctrl-Space': function ( cm ) {

	// 		server.complete( cm );

	// 	},
	// 	'Ctrl-I': function ( cm ) {

	// 		server.showType( cm );

	// 	},
	// 	'Ctrl-O': function ( cm ) {

	// 		server.showDocs( cm );

	// 	},
	// 	'Alt-.': function ( cm ) {

	// 		server.jumpToDef( cm );

	// 	},
	// 	'Alt-,': function ( cm ) {

	// 		server.jumpBack( cm );

	// 	},
	// 	'Ctrl-Q': function ( cm ) {

	// 		server.rename( cm );

	// 	},
	// 	'Ctrl-.': function ( cm ) {

	// 		server.selectName( cm );

	// 	}
	// } );

	// codemirror.on( 'cursorActivity', function ( cm ) {

	// 	if ( currentMode !== 'javascript' ) return;
	// 	server.updateArgHints( cm );

	// } );

	// codemirror.on( 'keypress', function ( cm, kb ) {

	// 	if ( currentMode !== 'javascript' ) return;
	// 	var typed = String.fromCharCode( kb.which || kb.keyCode );
	// 	if ( /[\w\.]/.exec( typed ) ) {

	// 		server.complete( cm );

	// 	}

	// } );

	//

	signals.editorCleared.add(function () {
		container.setDisplay("none");
	});

	signals.editSlide.add(function (slide) {
		var name, source;

		if (typeof slide === "object") {
			name = slide.name;
			source = slide.source;
			title.setValue("  " + name);
			objectUserData.setValue(slide.source);
			layoutSelect.setValue(slide.type);
			updateUI(objectUserData);
		} else {
			// switch ( script ) {
			// 	case 'vertexShader':
			// 		mode = 'glsl';
			// 		name = 'Vertex Shader';
			// 		source = object.material.vertexShader || "";
			// 		break;
			// 	case 'fragmentShader':
			// 		mode = 'glsl';
			// 		name = 'Fragment Shader';
			// 		source = object.material.fragmentShader || "";
			// 		break;
			// 	case 'programInfo':
			// 		mode = 'json';
			// 		name = 'Program Properties';
			// 		var json = {
			// 			defines: object.material.defines,
			// 			uniforms: object.material.uniforms,
			// 			attributes: object.material.attributes
			// 		};
			// 		source = JSON.stringify( json, null, '\t' );
			// }
			// title.setValue( object.material.name + ' / ' + name );
		}

		// currentMode = mode;
		currentSlide = slide;
		// currentObject = object;
		if (currentSlide != null) {
			let tmp = currentSlide.inAction;
			inActionSelect.setValue(tmp);
		}

		container.setDisplay("");
		updateInAnimation;
		// codemirror.setValue( source );
		// codemirror.clearHistory();
		// if ( mode === 'json' ) mode = { name: 'javascript', json: true };
		// codemirror.setOption( 'mode', mode );
	});

	signals.slideRemoved.add(function (slide) {
		if (currentSlide === slide) {
			container.setDisplay("none");
		}
	});

	signals.animationKeyAdded.add(updateInAnimation);
	signals.animationKeyChanged.add(updateInAnimation);
	signals.animationKeyRemoved.add(updateInAnimation);
	signals.objectChanged.add(updateInAnimation);

	function updateInAnimation() {
		let newOptions = {};
		let animationKeys = editor.animationKeys;
		for (var key in animationKeys) {
			var animationKey = animationKeys[key];

			if (
				animationKey.length === 0 ||
				editor.scene.getObjectByProperty("uuid", key) === undefined
			) {
				delete animationKeys[key];
			} else {
				for (var anim in animationKey) {
					newOptions["" + key + "@" + animationKey[anim].name] =
						"" +
						editor.scene.getObjectByProperty("uuid", key).name +
						"." +
						animationKey[anim].name;
				}
			}
		}
		if (newOptions == {}) {
			inActionRow.setDisplay("none");
		} else {
			inActionRow.setDisplay("");
		}
		inActionSelect.setOptions(newOptions);

		if (currentSlide != null) {
			let tmp = currentSlide.inAction;
			inActionSelect.setValue(tmp);
		}
	}

	return container;

	function updateUI(object) {
		if (object.getValue() != undefined) {
			uidiv.setValue(object.getValue());
		}
	}
};

export { Slide };
