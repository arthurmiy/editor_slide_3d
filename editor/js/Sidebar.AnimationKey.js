/**
 * @author mrdoob / http://mrdoob.com/
 */

import {
	UIPanel,
	UIBreak,
	UIText,
	UIButton,
	UIRow,
	UIInput,
} from "./libs/ui.js";

import { AddAnimationKeyCommand } from "./commands/AddAnimationKeyCommand.js";
import { SetAnimationKeyValueCommand } from "./commands/SetAnimationKeyValueCommand.js";
import { RemoveAnimationKeyCommand } from "./commands/RemoveAnimationKeyCommand.js";

var SidebarAnimationKey = function (editor) {
	var strings = editor.strings;

	var signals = editor.signals;

	var container = new UIPanel();
	container.setDisplay("none");

	container.add(new UIText("Animation Key").setTextTransform("uppercase"));
	container.add(new UIBreak());
	container.add(new UIBreak());

	//

	var animationKeysContainer = new UIRow();
	container.add(animationKeysContainer);

	var newAnimationKey = new UIButton(strings.getKey("sidebar/script/new"));
	newAnimationKey.onClick(function () {
		var tmp = editor.animationKeys[editor.selected.uuid];
		let index = tmp == undefined ? "0" : "" + tmp.length;

		/*




		define type
		position scale rotation opacity highlight






		*/
		var animationKey;
		if (index == "0") {
			animationKey = {
				name: "default",
				position: [0, 0, 0],
				rotation: [0, 0, 0],
				scale: [0, 0, 0],
				opacity: 1,
				highlight: 0,
				selected: {
					position: false,
					rotation: false,
					scale: false,
					opacity: false,
					highlight: false,
				},
			};
		} else {
			animationKey = {
				name: "key" + index,
				position: [0, 0, 0],
				rotation: [0, 0, 0],
				scale: [0, 0, 0],
				opacity: 1,
				highlight: 0,
				selected: {
					position: false,
					rotation: false,
					scale: false,
					opacity: false,
					highlight: false,
				},
			};
		}

		editor.execute(
			new AddAnimationKeyCommand(editor, editor.selected, animationKey)
		);
	});
	container.add(newAnimationKey);

	/*
	var loadScript = new UI.Button( 'Load' );
	loadScript.setMarginLeft( '4px' );
	container.add( loadScript );
	*/

	//

	function update() {
		animationKeysContainer.clear();
		animationKeysContainer.setDisplay("none");

		var object = editor.selected;

		if (object === null) {
			return;
		}

		var animationKeys = editor.animationKeys[object.uuid];

		if (animationKeys !== undefined && animationKeys.length > 0) {
			animationKeysContainer.setDisplay("block");

			for (var i = 0; i < animationKeys.length; i++) {
				(function (object, animationKey) {
					var name = new UIInput(animationKey.name)
						.setWidth("130px")
						.setFontSize("12px");
					name.onChange(function () {
						editor.execute(
							new SetAnimationKeyValueCommand(
								editor,
								editor.selected,
								animationKey,
								"name",
								this.getValue()
							)
						);
					});
					animationKeysContainer.add(name);

					var edit = new UIButton(strings.getKey("sidebar/script/edit"));
					edit.setMarginLeft("4px");
					edit.onClick(function () {
						signals.editAnimationKey.dispatch(object, animationKey);
					});
					animationKeysContainer.add(edit);

					var remove = new UIButton(strings.getKey("sidebar/script/remove"));
					remove.setMarginLeft("4px");
					remove.onClick(function () {
						if (confirm(strings.getKey("msg/confirm"))) {
							editor.execute(
								new RemoveAnimationKeyCommand(
									editor,
									editor.selected,
									animationKey
								)
							);
						}
					});
					animationKeysContainer.add(remove);

					animationKeysContainer.add(new UIBreak());
				})(object, animationKeys[i]);
			}
		}
	}

	// signals

	signals.objectSelected.add(function (object) {
		if (object !== null && editor.scene != object) {
			container.setDisplay("block");

			update();
		} else {
			container.setDisplay("none");
		}
	});

	signals.animationKeyAdded.add(update);
	signals.animationKeyRemoved.add(update);
	signals.animationKeyChanged.add(update);

	return container;
};

export { SidebarAnimationKey };
